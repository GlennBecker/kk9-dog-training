module Svgs exposing (checkSquare, heart, square, star)

import Html exposing (Html)
import Html.Attributes as HA
import Svg exposing (Svg)
import Svg.Attributes exposing (..)


star =
    Svg.svg [ viewBox "0 0 40 38.04" ]
        [ Svg.path
            [ fill "currentColor"
            , d "M20 29.44l12.36 8.6L28 23.63l12-9.1-15.06-.3L20 0l-4.94 14.22L0 14.53l12 9.1-4.36 14.41z"
            ]
            []
        ]


heart =
    Svg.svg
        [ viewBox "0 0 105.75 96.65" ]
        [ Svg.path
            [ fill "red"
            , d "M9.1 9.1a30.89 30.89 0 0 0 0 43.77l43.77 43.78 43.78-43.78a30.89 30.89 0 0 0 0-43.77 30.89 30.89 0 0 0-43.78 0 30.89 30.89 0 0 0-43.77 0z"
            ]
            []
        ]


svgFeatherIcon : String -> List (Svg msg) -> Html msg
svgFeatherIcon className =
    Svg.svg
        [ class <| "feather feather-" ++ className
        , fill "none"
        , height "24"
        , stroke "currentColor"
        , strokeLinecap "round"
        , strokeLinejoin "round"
        , strokeWidth "2"
        , viewBox "0 0 24 24"
        , width "24"
        ]


checkSquare : Html msg
checkSquare =
    svgFeatherIcon "check-square"
        [ Svg.polyline
            [ points "9 11 12 14 22 4" ]
            []
        , Svg.path
            [ d "M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11" ]
            []
        ]


square : Html msg
square =
    svgFeatherIcon "square"
        [ Svg.rect
            [ Svg.Attributes.x "3"
            , y "3"
            , width "18"
            , height "18"
            , rx "2"
            , ry "2"
            ]
            []
        ]
