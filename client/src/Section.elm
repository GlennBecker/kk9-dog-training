module Section exposing (view)

import Element exposing (..)
import Element.Background as Background
import Element.Font as Font
import Responsive
import Views exposing (whitespace)


view : Color -> Color -> List (Element msg) -> Int -> Element msg
view bgColor textColor content x =
    column
        [ width fill
        , height fill
        , Background.color bgColor
        , Font.color textColor
        , paddingXY (xPadding x) (yPadding x)
        , spacing (whitespace 4)
        ]
        content


xPadding =
    Responsive.scale
        ( 768, 1900 )
        ( toFloat (whitespace -2), toFloat (whitespace 15) )
        >> round


yPadding =
    Responsive.scale
        ( 768, 1900 )
        ( toFloat (whitespace 2), toFloat (whitespace 4) )
        >> round


whenJust x f =
    Maybe.map f x
        |> Maybe.withDefault none
