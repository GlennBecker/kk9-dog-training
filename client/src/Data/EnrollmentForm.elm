module Data.EnrollmentForm exposing
    ( AgeUnit
    , Form
    , ageUnitToAbbr
    , ageUnitToString
    , ageUnits
    , blank
    , encode
    )

import Data.ClassSchedule as ClassSchedule exposing (Class)
import Json.Encode as Encode


type AgeUnit
    = Weeks
    | Months
    | Years


ageUnits : List AgeUnit
ageUnits =
    [ Years
    , Months
    , Weeks
    ]


ageUnitToString : AgeUnit -> String
ageUnitToString unit =
    case unit of
        Weeks ->
            "weeks"

        Months ->
            "months"

        Years ->
            "years"


ageUnitToAbbr : AgeUnit -> String
ageUnitToAbbr unit =
    case unit of
        Weeks ->
            "wks"

        Months ->
            "mths"

        Years ->
            "yrs"


type alias Form =
    { dogsName : String
    , dogsAge : ( String, AgeUnit )
    , veterinarian : String
    , hasVaccinationPapers : Bool
    , behaviorIssues : String
    , first : String
    , last : String
    , streetAddress : String
    , unit : String
    , city : String
    , zipcode : String
    , email : String
    , phone : String
    , useMyPhotos : Bool
    , useDogsPhotos : Bool
    , agreeToTerms : Bool
    , subscribeToNewsletter : Bool
    }


blank : Form
blank =
    { dogsName = ""
    , dogsAge = ( "", Years )
    , veterinarian = ""
    , hasVaccinationPapers = False
    , behaviorIssues = ""
    , first = ""
    , last = ""
    , streetAddress = ""
    , unit = ""
    , city = ""
    , zipcode = ""
    , email = ""
    , phone = ""
    , useMyPhotos = False
    , useDogsPhotos = False
    , agreeToTerms = False
    , subscribeToNewsletter = False
    }


encode : Class -> String -> Form -> Encode.Value
encode class start form =
    Encode.object
        [ ( "className", Encode.string (ClassSchedule.className class) )
        , ( "startDate", Encode.string start )
        , ( "dogsName", Encode.string form.dogsName )
        , ( "dobOrAge", encodeAge form.dogsAge )
        , ( "veterinarian", Encode.string form.veterinarian )
        , ( "hasVaccinationPapers", Encode.bool form.hasVaccinationPapers )
        , ( "behaviorIssues", Encode.string form.behaviorIssues )
        , ( "first", Encode.string form.first )
        , ( "last", Encode.string form.last )
        , ( "streetAddress", Encode.string form.streetAddress )
        , ( "unit", Encode.string form.unit )
        , ( "city", Encode.string form.city )
        , ( "zipcode", Encode.string form.zipcode )
        , ( "email", Encode.string form.email )
        , ( "phone", Encode.string form.phone )
        , ( "useMyPhotos", Encode.bool form.useMyPhotos )
        , ( "useDogsPhotos", Encode.bool form.useDogsPhotos )
        , ( "agreeToTerms", Encode.bool form.agreeToTerms )
        , ( "subscribeToNewsletter", Encode.bool form.subscribeToNewsletter )
        ]


encodeAge : ( String, AgeUnit ) -> Encode.Value
encodeAge ( age, unit ) =
    Encode.string (age ++ " " ++ ageUnitToString unit)
