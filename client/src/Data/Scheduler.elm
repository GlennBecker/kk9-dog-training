module Data.Scheduler exposing (buildSchedule, makeUpClass, noMakeUpClass)

import Date exposing (Date, Unit(..))
import Html
import Time exposing (Month(..), Weekday(..))


start : Date -> Date
start =
    identity


week : Date -> Date
week =
    Date.add Weeks 1


skip : Date -> Date
skip =
    Date.add Weeks 2


zero x =
    ( x, [] )


lift : (a -> b) -> a -> ( b, List b )
lift f x =
    f x |> (\y -> ( y, [ y ] ))


compose : (a -> ( b, List d )) -> (b -> ( c, List d )) -> a -> ( c, List d )
compose f g x =
    let
        ( y, ys ) =
            f x

        ( z, zs ) =
            g y
    in
    ( z, zs ++ ys )


makeUpClass : Date -> Maybe Date
makeUpClass =
    Date.add Weeks 1 >> Just


noMakeUpClass : Date -> Maybe Date
noMakeUpClass =
    always Nothing


flip f x y =
    f y x


buildSchedule : Int -> List Int -> (Date -> ( Date, List Date ))
buildSchedule numberOfClasses skipWeeks =
    let
        toDateFunc x =
            if x == 1 then
                lift start

            else if List.member x skipWeeks then
                lift skip

            else
                lift week
    in
    List.range 1 numberOfClasses
        |> List.map toDateFunc
        |> List.foldr compose zero


main =
    Date.fromCalendarDate 2019 Mar 3
        |> buildSchedule 5 [ 2, 4 ]
        |> Tuple.second
        |> List.reverse
        |> List.map (Date.toIsoString >> Html.text >> List.singleton >> Html.div [])
        |> Html.div []
