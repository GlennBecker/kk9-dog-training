module Data.ContactInfo exposing
    ( ContactInfo
    , kk9Contact
    , printBusinessName
    , printCityStateZip
    , printPhoneNumber
    , printStreetAddress
    )


type alias ContactInfo =
    { business : String
    , street : ( Int, String )
    , unit : Maybe String
    , city : String
    , state : String
    , zipcode : Int
    , phone : ( Int, Int, Int )
    , facebookUrl : String
    }


kk9Contact : ContactInfo
kk9Contact =
    { business = "KK9 Dog Training"
    , street = ( 136, "West 4th Street" )
    , unit = Nothing
    , city = "Loveland"
    , state = "CO"
    , zipcode = 80537
    , phone = ( 970, 412, 7898 )
    , facebookUrl = "https://www.facebook.com/KK9dogtraining/"
    }


printBusinessName : ContactInfo -> String
printBusinessName { business } =
    business ++ ", " ++ "LLC"


printStreetAddress : ( Int, String ) -> Maybe String -> String
printStreetAddress ( number, streetInfo ) unit =
    let
        address =
            String.fromInt number ++ " " ++ streetInfo
    in
    unit
        |> Maybe.map ((++) (address ++ " "))
        |> Maybe.withDefault address


printPhoneNumber : ( Int, Int, Int ) -> String
printPhoneNumber ( area, prefix, suffix ) =
    String.join ""
        [ "("
        , String.fromInt area
        , ")"
        , " "
        , String.fromInt prefix
        , "-"
        , String.fromInt suffix
        ]


printCityStateZip : ContactInfo -> String
printCityStateZip { city, state, zipcode } =
    city ++ ", " ++ String.toUpper state ++ ", " ++ String.fromInt zipcode
