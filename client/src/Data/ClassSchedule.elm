module Data.ClassSchedule exposing
    ( Class(..)
    , ClassData
    , ClassType(..)
    , Description
    , Schedule
    , Time
    , className
    , classTypeQuery
    , classes
    , describe
    , exists
    , existsOngoing
    , getClassDates
    , getDates
    , getDatesFor
    , getSubclassesFor
    , isCLASS
    , parseTimeUrl
    , runningClasses
    , selfSelection
    , startDate
    , timeToString
    , toList
    , toUrlString
    )

import AssocList as Dict exposing (Dict)
import Date exposing (..)
import DateHelp exposing (weekDayString)
import Html exposing (..)
import Parser as P exposing ((|.), (|=))
import Time exposing (Month(..), Weekday(..))


type Time
    = AM Int Int
    | PM Int Int


timeToString : Time -> String
timeToString time =
    case time of
        AM hr min ->
            String.fromInt hr ++ printMin min ++ " a.m."

        PM hr min ->
            String.fromInt hr ++ printMin min ++ " p.m."


printMin : Int -> String
printMin min =
    if min == 0 then
        ""

    else if min < 10 then
        ":0" ++ String.fromInt min

    else
        ":" ++ String.fromInt min


timeUrl : Time -> String
timeUrl =
    timeToString
        >> String.replace "m." ""
        >> String.replace "." ""
        >> String.replace " " ""


parseTimeUrl : String -> Result (List P.DeadEnd) ( Weekday, Time )
parseTimeUrl =
    (P.succeed (\day time f -> ( day, time, f ))
        |= (P.chompWhile Char.isAlpha
                |> P.getChompedString
                |> P.andThen
                    (\w ->
                        case DateHelp.weekdayFromString w of
                            Just x ->
                                P.succeed x

                            _ ->
                                P.problem "bad weekday"
                    )
           )
        |= P.int
        |= P.oneOf
            [ P.map (\_ -> PM) (P.chompIf ((==) 'p'))
            , P.map (\_ -> AM) (P.chompIf ((==) 'a'))
            ]
        |. P.end
    )
        |> P.andThen parseTimeHelp
        |> P.run


parseTimeHelp : ( Weekday, Int, Int -> Int -> Time ) -> P.Parser ( Weekday, Time )
parseTimeHelp ( day, time, f ) =
    P.succeed ( day, f time 0 )


type Class
    = PuppyPrimer
    | MannersMatter1
    | MannersMatter2
    | ShortAndSweet
    | CLASS1
    | CLASS2
    | AttentionPlease
    | LeashManners
    | PoliteGreetings


isCLASS : Class -> Bool
isCLASS class =
    case class of
        CLASS1 ->
            True

        CLASS2 ->
            True

        _ ->
            False


type ClassType
    = Full Time Date (List Date) (Maybe Date)
    | Mini Time Date (List Date)
    | Ongoing Time Weekday


isExpired : Date -> ClassType -> Bool
isExpired date classType =
    case classType of
        Full _ start _ _ ->
            case Date.compare start date of
                Basics.LT ->
                    True

                _ ->
                    False

        Mini _ start _ ->
            case Date.compare start date of
                Basics.LT ->
                    True

                _ ->
                    False

        Ongoing _ _ ->
            False


existsOngoingDate ( time, weekday ) date =
    case date of
        Ongoing w t ->
            w == weekday && t == time

        _ ->
            False


startDate classType =
    case classType of
        Full _ date _ _ ->
            Date.toIsoString date

        Mini _ date _ ->
            Date.toIsoString date

        Ongoing time weekday ->
            timeToString time
                |> String.replace " " ""
                |> String.replace "." ""
                |> (++) (String.toLower (weekDayString weekday) ++ "_")


classTypeQuery classType =
    case classType of
        Full _ date _ _ ->
            Date.toIsoString date

        Mini _ date _ ->
            Date.toIsoString date

        Ongoing time weekday ->
            String.toLower (DateHelp.weekDayAbbr weekday) ++ timeUrl time


equalDates date classType =
    case classType of
        Full _ start _ _ ->
            date == start

        Mini _ start _ ->
            date == start

        Ongoing _ _ ->
            False


compareDates c1 c2 =
    case ( c1, c2 ) of
        ( Full _ d1 _ _, Full _ d2 _ _ ) ->
            Date.compare d1 d2

        ( Mini _ d1 _, Mini _ d2 _ ) ->
            Date.compare d1 d2

        _ ->
            Basics.EQ


type Schedule
    = Schedule (Dict Class (List ClassType))


exists class date (Schedule schedule) =
    schedule
        |> Dict.get class
        |> Maybe.map (List.filter (equalDates date) >> List.isEmpty >> not)
        |> Maybe.withDefault False


existsOngoing class dayTime (Schedule schedule) =
    schedule
        |> Dict.get class
        |> Maybe.map (List.filter (existsOngoingDate dayTime) >> List.isEmpty >> not)
        |> Maybe.withDefault False



-- SCHEDULE


runningClasses : Date -> Schedule
runningClasses date =
    [ puppyPrimer
    , mannersMatter1
    , shortAndSweet
    , class1
    , class2
    , politeGreetings
    , leashManners
    , attentionPlease
    ]
        |> Dict.fromList
        |> Dict.map (\_ d -> List.filter (isExpired date >> not) d)
        |> Schedule


runningCLASSdates date =
    [ class1, class2 ]
        |> Dict.fromList
        |> Dict.map (\_ d -> List.filter (isExpired date >> not) d)
        |> Schedule


puppyPrimer : ( Class, List ClassType )
puppyPrimer =
    [ Ongoing (PM 12 0) Mon
    , Ongoing (PM 12 0) Sat
    ]
        |> Tuple.pair PuppyPrimer


mannersMatter1 : ( Class, List ClassType )
mannersMatter1 =
    [ full (PM 6 0) (Date.fromCalendarDate 2020 Jun 23) []
    , full (PM 6 0) (Date.fromCalendarDate 2020 Jul 1) []
    ]
        |> List.sortWith compareDates
        |> Tuple.pair MannersMatter1


shortAndSweet : ( Class, List ClassType )
shortAndSweet =
    []
        |> List.sortWith compareDates
        |> Tuple.pair ShortAndSweet


class1 =
    [ full (PM 5 0) (Date.fromCalendarDate 2020 Mar 16) []
    , full (PM 5 0) (Date.fromCalendarDate 2020 May 10) []
    ]
        |> Tuple.pair CLASS1


class2 =
    [ full (AM 9 0) (Date.fromCalendarDate 2020 May 25) []
    , full (PM 6 0) (Date.fromCalendarDate 2020 Jul 9) []
    ]
        |> Tuple.pair CLASS2


politeGreetings =
    [ mini (PM 4 0) (Date.fromCalendarDate 2020 Jul 5) []
    ]
        |> Tuple.pair PoliteGreetings


leashManners =
    [ mini (PM 4 0) (Date.fromCalendarDate 2020 Jul 26) []
    ]
        |> Tuple.pair LeashManners


attentionPlease =
    [ mini (PM 4 0) (Date.fromCalendarDate 2020 Jun 14) []
    ]
        |> Tuple.pair AttentionPlease



-- CONSTRUCTORS


full : Time -> Date -> List Int -> ClassType
full time start skipWeeks =
    let
        ( dates, makeUp ) =
            getClassDates 6 start skipWeeks False
    in
    Full time start (List.reverse dates) makeUp


mini : Time -> Date -> List Int -> ClassType
mini time start skipWeeks =
    let
        ( dates, _ ) =
            getClassDates 3 start skipWeeks False
    in
    Mini time start dates


getDates : ClassType -> List Date
getDates classType =
    case classType of
        Full _ start rest _ ->
            start :: rest

        Mini _ start rest ->
            start :: rest

        Ongoing _ _ ->
            []


getSubclasses class =
    case class of
        ShortAndSweet ->
            [ AttentionPlease
            , LeashManners
            , PoliteGreetings
            ]

        _ ->
            []



-- ACCESSORS


getDatesFor : Class -> Schedule -> List ClassType
getDatesFor class (Schedule schedule) =
    schedule
        |> Dict.get class
        |> Maybe.withDefault []


getSubclassesFor class schedule =
    getSubclasses class
        |> List.map (\c -> ( c, getDatesFor c schedule ))
        |> Dict.fromList


classes : Schedule -> List Class
classes (Schedule schedule) =
    schedule |> Dict.keys


toList : Schedule -> List ( Class, List ClassType )
toList (Schedule schedule) =
    schedule |> Dict.toList



-- CLASS DATA


type alias ClassData =
    { classType : String
    , time : Time
    , start : Date
    , dates : List Date
    , makeUp : Maybe Date
    }



-- CLASS DATA


toUrlString class =
    case class of
        PuppyPrimer ->
            "puppyprimer"

        MannersMatter1 ->
            "mannersmatter"

        MannersMatter2 ->
            "mannersmatter2"

        ShortAndSweet ->
            "shortandsweet"

        AttentionPlease ->
            "attentionplease"

        LeashManners ->
            "looseleashwalking"

        PoliteGreetings ->
            "politegreetings"

        CLASS1 ->
            "class1"

        CLASS2 ->
            "class2"


fromUrlString class =
    case class of
        "puppyprimer" ->
            Just PuppyPrimer

        "mannersmatter" ->
            Just MannersMatter1

        "mannersmatter2" ->
            Just MannersMatter2

        "shortandsweet" ->
            Just ShortAndSweet

        "class1" ->
            Just CLASS1

        "class2" ->
            Just CLASS2

        _ ->
            Nothing


className class =
    case class of
        PuppyPrimer ->
            "Puppy Primer"

        MannersMatter1 ->
            "Manners Matter"

        MannersMatter2 ->
            "Manners Matter 2"

        ShortAndSweet ->
            "Short And Sweet"

        CLASS1 ->
            "CLASS H.S. and B.A. Prep"

        CLASS2 ->
            "CLASS B.A. and M.A. Prep"

        LeashManners ->
            "Loose-leash Walking"

        AttentionPlease ->
            "Attention Please!"

        PoliteGreetings ->
            "Polite Greetings"



-- SCHEDULER


zero : Date -> ( Date, List Date )
zero x =
    ( x, [] )


week : Date -> Date
week =
    Date.add Weeks 1


skip : Date -> Date
skip =
    Date.add Weeks 2


getClassDates sessions start skips hasMakeup =
    let
        go x acc =
            let
                date =
                    Date.add Weeks x start
            in
            date :: acc

        finish dates =
            if hasMakeup then
                List.head dates
                    |> Maybe.map (Date.add Weeks 1)
                    |> Tuple.pair (List.reverse dates)

            else
                ( dates, Nothing )
    in
    List.range 2 sessions
        |> getOffsets skips
        |> List.foldl go []
        |> finish


getOffsets skips weeks =
    let
        offsetWeek weekNumber =
            skips
                |> List.map
                    (\x ->
                        if weekNumber >= x then
                            1

                        else
                            0
                    )
                |> List.sum
                |> (+) (weekNumber - 1)
    in
    List.map offsetWeek weeks



-- DESCRIBE


type alias Description =
    { title : String
    , forAges : String
    , blurb : String
    , description : List String
    , skills : List String
    , includes : List String
    , prerequisites : Maybe (List String)
    }


noDescription =
    { title = ""
    , forAges = ""
    , blurb = ""
    , description = []
    , skills = []
    , includes = []
    , prerequisites = Nothing
    }


withTitle title description =
    { description | title = title }


describe : Class -> Description
describe class =
    case class of
        PuppyPrimer ->
            { title = "Puppy Primer"
            , forAges = "Ages 8 to 16 weeks"
            , blurb = "Socialization and foundation training for your 8-16 week old puppy."
            , description = [ "In a safe, friendly class setting your puppy will socialize with other puppies and people and interact with various toys, textures, sounds, novelties." ]
            , includes = [ "Clicker", "Class Notes" ]
            , skills =
                [ "Watch me / Look"
                , "Sit"
                , "Drop-it / Trade"
                , "Introduce Leave-it"
                , "Come When Called"
                ]
            , prerequisites = Nothing
            }

        MannersMatter1 ->
            { title = "Manners Matter"
            , forAges = "All Ages!"
            , blurb = "The road to a happy life with your dog starts with basic obedience training."
            , description =
                [ "In this class we start you and your dog off with all the building blocks you'll need to have a happy, respectful and loving relationship."
                ]
            , includes = [ "Clicker" ]
            , skills =
                [ "Watch me / Look"
                , "Sit"
                , "Down"
                , "Stay"
                , "Drop-it / Trade"
                , "Leave-it"
                , "Come when called"
                , "Wait at door"
                ]
            , prerequisites = Nothing
            }

        MannersMatter2 ->
            { title = "Manners Matter 2"
            , forAges = "All Ages!"
            , blurb = "The next step in obedience mastery."
            , description =
                [ "In this class the basic skills from Manners Matter 1 will become more reliable as we add the 3 D's: Distance, Duration, Distraction"
                ]
            , includes = [ "Clicker", "Email Notes" ]
            , skills =
                [ "Go To Place"
                , "Target"
                , "Settle"
                , "Leash Manners"
                , "Refining Manners Matter 1 skills"
                ]
            , prerequisites = Just [ "Manners Matter 1" ]
            }

        ShortAndSweet ->
            { title = "Short And Sweet"
            , forAges = "All Ages!"
            , blurb = "Learning to learn"
            , description =
                [ "3 week beginning class for any age dog."
                ]
            , includes = [ "Clicker", "Email Notes" ]
            , skills =
                [ "Sit"
                , "Down"
                , "Stay"
                , "Wait at door"
                , "Leave it"
                , "Come when called"
                ]
            , prerequisites = Just [ "Manners Matter 1" ]
            }

        CLASS1 ->
            noDescription

        CLASS2 ->
            noDescription

        LeashManners ->
            noDescription
                |> withTitle (className LeashManners)

        PoliteGreetings ->
            noDescription
                |> withTitle "Polite Greetings"

        AttentionPlease ->
            noDescription
                |> withTitle "Attention Please!"


selfSelection class =
    case class of
        PuppyPrimer ->
            [ "Do you have a puppy who is between 8 and 16 weeks old?"
            , "Does your puppy behave like an angel… until he/she doesn’t?"
            , "Does your puppy play appropriately… until he/she doesn’t?"
            , "Do you wish you had more strategies to help your puppy get the exercise and enrichment he/she needs without it taking up your entire day?"
            ]

        MannersMatter1 ->
            [ "Do you have a young dog who needs to learn house manners?"
            , "Does your dog pull you down the street?"
            , "Does your dog do what you want sometimes and not others?"
            , "Are you stressed when someone comes to your door?"
            , "Do you wish your dog would just “listen”?"
            ]

        MannersMatter2 ->
            [ "Does your dog know basic commands?"
            , "Do you want to take your training to the next level?"
            , "Does your dog do well with some behaviors but not others?"
            , "Does having accountability help you to stay on track with your training?"
            ]

        ShortAndSweet ->
            [ "Busy Schedule?", "Can't commit to a five week class?", "Need manners quickly?" ]

        CLASS1 ->
            []

        CLASS2 ->
            []

        _ ->
            []
