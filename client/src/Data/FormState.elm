module Data.FormState exposing (FormState(..), errors)

import Http


type FormState a
    = Filling (List ( a, String ))
    | Sending
    | Sent
    | SendingError Http.Error


errors state =
    case state of
        Filling errs ->
            errs

        _ ->
            []
