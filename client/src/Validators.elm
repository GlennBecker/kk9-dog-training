module Validators exposing (firstAndLast, requireLength, usPhoneNumber, validContact, validate)

import Validate exposing (Validator)


firstAndLast name =
    List.length (String.split " " name) > 1


requireLength length =
    String.length >> (==) length


usPhoneNumber =
    String.filter Char.isDigit
        >> String.length
        >> (==) 10


emailOrPhone =
    Validate.fromErrors


validate : (a -> Bool) -> (subject -> a) -> error -> Validator error subject
validate pred getValue error =
    let
        func x =
            if pred x then
                []

            else
                [ error ]
    in
    Validate.fromErrors (getValue >> func)


or : Validator a b -> Validator a b -> Validator a b
or v1 v2 =
    let
        newValidator x =
            case Validate.validate v1 x of
                Ok valid ->
                    []

                Err _ ->
                    case Validate.validate v2 x of
                        Ok valid ->
                            []

                        Err errors ->
                            errors
    in
    Validate.fromErrors newValidator


validContact toField error =
    or
        (validate usPhoneNumber toField error)
        (Validate.ifInvalidEmail toField (always error))
