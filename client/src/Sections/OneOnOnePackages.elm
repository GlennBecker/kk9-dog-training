module Sections.OneOnOnePackages exposing (view)

import Element exposing (..)
import Element.Background as Background
import Element.Font as Font
import Responsive
import Text
import Views exposing (palette, whitespace)


type alias Url =
    String


type alias OneOnOnePackage =
    { title : String
    , description : List String
    , price : Int
    , disclaimer : Maybe String
    , furtherInfo : Maybe ( Url, String )
    }


packages : List OneOnOnePackage
packages =
    [ { title = "Private Targeted Training"
      , description =
            [ "Dogs of any age/stage"
            , "Individual training sessions to focus and target behavior that is driving you crazy!"
            , "2, 3, and 4 sessions packages"
            ]
      , price = 25
      , disclaimer = Nothing
      , furtherInfo = Nothing
      }
    , { title = "Reactive Dog Training & Support"
      , description =
            [ "Fear-based aggression"
            , "Leash reactivity"
            , "Unpredictable behaviors"
            ]
      , price = 160
      , disclaimer = Nothing
      , furtherInfo = Nothing
      }
    , { title = "Training And Enrichment Walks"
      , description =
            [ "In home visit And walk"
            , "Build loose-leash skills"
            , "Provide enrichment activities"
            ]
      , price = 160
      , disclaimer = Nothing
      , furtherInfo = Nothing
      }
    ]


view : Int -> Element msg
view x =
    column [ width fill, centerX, spacing (whitespace 1) ]
        [ column
            [ width fill
            , padding (whitespace 2)
            , centerX
            , Background.color palette.brown
            , Font.color palette.white
            , Font.center
            , spacing (whitespace 1)
            ]
            [ Text.h2 "One-On-One Training" x
            ]
        , Responsive.switch 600
            ( column, row )
            x
            [ width fill, spacing (whitespace 1), centerX ]
            (List.map (viewOneOnOnePackage x) packages)
        ]


viewOneOnOnePackage : Int -> OneOnOnePackage -> Element msg
viewOneOnOnePackage x { title, description } =
    let
        cardWidth =
            Responsive.callOut
                [ Responsive.minWidth 480 (width fill)
                ]
                |> Responsive.defaultTo (width fill)
    in
    column
        [ cardWidth x
        , height fill
        , centerX
        , alignTop
        , spacing (whitespace 1)
        , padding (whitespace 1)
        , Font.center
        , Background.color palette.white
        , Views.roundedCorners
        , Views.shadow
        ]
        [ el
            [ width fill
            , Font.color Views.palette.white
            , Views.fontShadow
            , Background.color Views.palette.base
            , padding 16
            ]
            (Text.h2 title x)
        , el [ centerX, Font.alignLeft, width (maximum 300 fill) ] <|
            Text.bodyBlock (List.map ((++) (String.fromList [ '•', ' ' ])) description)
        , Element.link [ alignBottom, centerX ]
            { url = "https://square.site/book/8WAAPT7YWF5PK/kk9-dog-training-loveland-co"
            , label = Views.linkButton "Book An Appointment"
            }
        , el [ centerX ] <| Text.body "Or Call Kay to Schedule: (970) 412-7898"
        ]
