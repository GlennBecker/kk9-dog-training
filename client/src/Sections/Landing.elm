module Sections.Landing exposing (view)

import Element exposing (..)
import Element.Background as Background
import Element.Font as Font
import Text
import Views exposing (palette, whitespace)


view x y =
    column [ width fill ]
        [ strip 2 palette.brown <|
            link [ centerX ]
                { url = "/"
                , label =
                    column
                        [ Font.center, spacing 8 ]
                        [ Text.h1 "KK9 Dog Training" x
                        , Text.body "Gentle Training For Dogs & The People Who Love Them"
                        ]
                }
        , strip 1
            palette.base
            (Text.h3 "(970) 412-7898" x)
        ]


strip : Int -> Element.Color -> Element msg -> Element msg
strip paddingAmount color =
    el
        [ width fill
        , Background.color color
        , Font.color palette.white
        , Font.center
        , padding (whitespace paddingAmount)
        ]
