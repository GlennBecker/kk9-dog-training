module Sections.Footer exposing (view)

import Data.ContactInfo as Contact exposing (kk9Contact)
import Element exposing (..)
import Element.Background as Background
import Element.Font as Font
import Html
import Html.Attributes as HA
import Responsive
import Section
import Text
import Views exposing (palette, whitespace)


view x =
    el
        [ Background.color palette.black
        , Font.color palette.white
        , width fill
        , paddingXY (whitespace 2) (whitespace 6)
        ]
    <|
        Responsive.rowToColumn 950
            x
            [ centerX
            , spacing (whitespace 5)
            ]
            [ column [ alignTop, spacing (whitespace 3), centerX ]
                [ viewContactInfo
                , link
                    [ Font.center
                    , centerX
                    , Font.color palette.accent
                    ]
                    { url = kk9Contact.facebookUrl
                    , label = text "Follow Us On Facebook!"
                    }
                ]
            , viewbusinessHours
            , el [ alignTop ] (html (viewMap x))
            ]


viewMap x =
    Html.iframe
        [ HA.src "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d97166.21005297953!2d-105.13457332349279!3d40.443307485539435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87695387cd943827%3A0xbffd4bd4cd10f763!2sKK9+Dog+Training+LLC!5e0!3m2!1sen!2sus!4v1541208264891"
        , HA.width 288
        , HA.height 225
        , HA.style "border" "0"
        , HA.style "frameborder" "0"
        , HA.title "Location Map"
        ]
        []


mapWidth =
    Responsive.scale ( 600, 1100 ) ( 288, 600 ) >> round


mapHeight =
    Responsive.scale ( 600, 1100 ) ( 225, 450 ) >> round


viewContactInfo =
    column
        [ spacing (whitespace -1), Font.color palette.white, centerX, Font.center ]
        [ Text.body (Contact.printBusinessName kk9Contact)
        , Text.body (Contact.printStreetAddress kk9Contact.street kk9Contact.unit)
        , Text.body (Contact.printCityStateZip kk9Contact)
        , Text.body (Contact.printPhoneNumber kk9Contact.phone)
        ]


type TimeOfDay
    = AM Int
    | PM Int


type Hours
    = OpenBetween TimeOfDay TimeOfDay
    | Closed


businessHours =
    { mon = OpenBetween (PM 4) (PM 7)
    , tues = OpenBetween (PM 4) (PM 7)
    , wed = Closed
    , thurs = Closed
    , fri = OpenBetween (PM 4) (PM 7)
    , sat = OpenBetween (AM 9) (PM 5)
    , sun = OpenBetween (PM 1) (PM 5)
    }


printHours hours =
    case hours of
        OpenBetween start end ->
            printTimeOfDay start ++ " - " ++ printTimeOfDay end

        Closed ->
            "Closed"


printTimeOfDay tod =
    case tod of
        AM time ->
            String.fromInt time ++ " AM"

        PM time ->
            String.fromInt time ++ " PM"


viewbusinessHours =
    column [ spacing (whitespace -1), alignTop, width (minimum 250 fill), Font.center ]
        [ el [ Font.bold, width fill ] (Text.body "Business Hours")
        , Text.body <| "Tuesday-Sunday by appointment"
        ]
