module Sections.ContactForm exposing (Config, view)

import Data.ContactInfo as Contact exposing (kk9Contact)
import Data.FormState as FormState exposing (FormState(..))
import Element exposing (..)
import Element.Background as Background
import Element.Font as Font
import Element.Input as Input
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Section
import Text
import Validate exposing (Valid)
import Validators as V
import Views exposing (palette, whitespace)


type alias Config msg =
    { onEmail : String -> msg
    , onFirst : String -> msg
    , onLast : String -> msg
    , onQuestion : String -> msg
    , onSubmit : msg
    }


type alias Form r =
    { r | first : String, last : String, email : String, questions : String }


view : Config msg -> FormState String -> Form r -> Int -> Element msg
view config state form x =
    case state of
        Filling errs ->
            Section.view
                palette.base
                palette.white
                [ column [ width (maximum 600 fill), spacing (whitespace 2), centerX ]
                    [ Text.h1 "Get In Touch" x
                    , el
                        [ centerX ]
                        (Text.h3 "Questions? We've got answers." x)
                    , viewContactForm config form errs
                    , el
                        [ centerX ]
                        (Text.body ("Or Call Us At " ++ Contact.printPhoneNumber kk9Contact.phone))
                    ]
                ]
                x

        Sending ->
            Section.view
                palette.base
                palette.white
                [ column [ width (maximum 600 fill), spacing (whitespace 2), centerX ]
                    [ Text.h1 "Sending your message..." x
                    ]
                ]
                x

        Sent ->
            Section.view
                palette.base
                palette.white
                [ column [ width (maximum 600 fill), spacing (whitespace 2), centerX ]
                    [ Text.h1 "Terrific!" x
                    , el
                        [ centerX ]
                        (Text.h3 "We'll be in touch shortly" x)
                    ]
                ]
                x

        SendingError _ ->
            Section.view
                palette.base
                palette.white
                [ column [ width (maximum 600 fill), spacing (whitespace 2), centerX ]
                    [ Text.h1 "Whoops" x
                    , el [ centerX ] (Text.h3 "Something went wrong." x)
                    , el
                        [ centerX ]
                        (Text.body ("Please contact us at " ++ Contact.printPhoneNumber kk9Contact.phone ++ "or try again"))
                    , el [ centerX, width (maximum 320 fill) ] (Views.actionButton config.onSubmit "Try Again")
                    ]
                ]
                x


input : (String -> msg) -> String -> String -> List ( String, String ) -> Element msg
input onChange field text errs =
    Input.text
        [ Font.alignLeft
        , Font.color palette.black
        ]
        { onChange = onChange
        , text = text
        , placeholder = Just (Input.placeholder [] <| Element.text field)
        , label = labelOrError field errs
        }


messageInput onChange field text errors =
    Input.multiline
        [ Font.alignLeft
        , Font.color palette.black
        , height (px (whitespace 10))
        ]
        { onChange = onChange
        , text = text
        , placeholder = Just (Input.placeholder [] (Element.text "Questions or behavior concerns ..."))
        , label = labelOrError field errors
        , spellcheck = True
        }


labelOrError field errors =
    let
        error =
            errors
                |> List.filter (\( f, _ ) -> field == f)
                |> List.head
    in
    case error of
        Just ( _, err ) ->
            Input.labelAbove [ Font.color (rgb 1 0 0) ] (Text.body err)

        Nothing ->
            Input.labelHidden field


viewContactForm : Config msg -> Form r -> List ( String, String ) -> Element msg
viewContactForm config form errs =
    column
        [ width (maximum 450 fill)
        , padding (whitespace 1)
        , spacing (whitespace 1)
        , centerX
        , Background.color palette.white
        , Views.roundedCorners
        , Views.shadow
        ]
        [ row [ spacing 8 ]
            [ input config.onFirst "First Name" form.first errs
            , input config.onLast "Last Name" form.last errs
            ]
        , input config.onEmail "Email" form.email errs
        , messageInput config.onQuestion "Message" form.questions errs
        , Views.actionButton config.onSubmit "Submit Question"
        ]
