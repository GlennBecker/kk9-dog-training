module Sections.CovidBanner exposing (view)

import Colors as Color exposing (base, white)
import Element exposing (Element, fill)
import Element.Font as Font
import Views exposing (linkButton)


view : Int -> Element msg
view x =
    getContainer x
        [ Element.paddingXY 24 8
        , getSpacing x
        , Element.width fill
        , Color.background base
        , Color.font white
        ]
        [ Element.paragraph [ Font.heavy ] [ Element.text "A Message From Us About Covid-19" ]
        , Element.link [ Element.width (Element.maximum 160 fill) ]
            { url = "/covid-19-update"
            , label = linkButton "Read More"
            }
        ]


getContainer x =
    if x < 900 then
        Element.column

    else
        Element.row


getSpacing x =
    if x < 900 then
        Element.spacing 16

    else
        Element.spaceEvenly
