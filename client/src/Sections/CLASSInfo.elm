module Sections.CLASSInfo exposing (view)

import Colors
import Element exposing (Element, fill)
import Element.Background as Background
import Element.Font as Font
import Text
import Views


view : Int -> Element msg
view x =
    Element.el
        [ Font.center
        , Element.width fill
        , Element.spacing 16
        , Element.height fill
        , Element.paddingXY 16 32
        , Background.image "src/Assets/adoptionoptions.jpg"
        ]
    <|
        Element.column [ Element.spacing 16, Element.centerX ]
            [ Element.column
                [ Colors.background Colors.white
                , Element.padding 16
                , Element.width fill
                , Element.spacing 16
                , Views.shadow
                ]
                [ Element.column [ Element.width fill, Element.spacing 8 ]
                    [ header
                        { color = Colors.brown
                        , content = Text.h2 "C.L.A.S.S." x
                        }
                    , header
                        { color = Colors.base
                        , content = Text.h3 "Canine Life and Social Skills" x
                        }
                    ]
                , Element.column [ Element.centerX, Element.spacing 16 ]
                    [ Text.bodyBlock
                        [ "Curriculum developed by Association of Professional Dog Trainers aligns with AKC S.T.A.R. Puppy, Canine Good Citizen and Therapy Dog Prep."
                        , "There are 4 levels of C.L.A.S.S. your dog can earn."
                        , "Each level has specific benchmarks, homework and assessments."
                        ]
                    , Element.link
                        [ Element.centerX, Element.width (Element.maximum 300 fill) ]
                        { url = "/class"
                        , label = Views.linkButton "Learn More About C.L.A.S.S."
                        }
                    ]
                ]
            ]


header { color, content } =
    Element.el
        [ Element.padding 16
        , Element.width fill
        , Colors.background color
        , Colors.font Colors.white
        ]
        content
