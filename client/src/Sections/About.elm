module Sections.About exposing (view)

import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Html exposing (Html)
import Responsive as R
import Section
import Text
import Views exposing (palette, whitespace)


about =
    { name = "Kay Williams"
    , imgUrl = "src/Assets/Profile_S.jpg"
    , signature = ""
    , header = "Does your dog know what to do?"
    , about =
        [ "We know what we want our dogs NOT to do so let's teach them what is ok to do instead!"
        , "I'm Kay Williams, owner of KK9 Dog Training."
        , "Because I have dealt with frustrating behaviors in my own dogs, I have a great deal of empathy for owners who may be discouraged with their dog in spite of loving them very much."
        , "Positive, force-free training methods are my focus. Knowing how to communicate so that the student understands (canine or human), is the foundation of all good teaching. This kind of learning is fun for dogs and their people."
        , "My background in education helps me to tailor instruction to individuals. I have extensive experience in differentiated instruction which has helped me teach to the individual client. I have a great deal of patience and am willing to persevere with dogs and owners."
        , "While there are common traits and learning principles for canines and humans, each dog and owner is unique. I love to discover what works for each individual and to gently train for success!"
        , "I look forward to working with you and your companion."
        ]
    }


view : Int -> Element msg
view x =
    Section.view
        palette.white
        palette.black
        [ Text.h1 "About KK9 Dog Training" x
        , R.switch 1200
            ( column, row )
            x
            [ centerX, spacing (whitespace 4) ]
            [ letter_ x
            , viewCertifications x
            ]
        ]
        x


letter_ x =
    R.switch 800
        ( column, textColumn )
        x
        [ spacing (whitespace 1), centerX ]
        [ el [ width fill, R.switch 800 ( Font.center, Font.alignLeft ) x ] (Text.h2 about.header x)
        , image
            [ imageAlignment x, width (px (imageWidth x)) ]
            { src = about.imgUrl
            , description = about.name
            }
        , Text.bodyBlock about.about
        ]


imageAlignment =
    R.switch 800 ( centerX, alignLeft )


imageWidth =
    R.scale ( 700, 1000 ) ( 300, 350 ) >> round


type alias Certification =
    { organization : String
    , certifications : List String
    }


allCertifications : List Certification
allCertifications =
    [ { organization = "Lesley University"
      , certifications = [ "Masters in Education" ]
      }
    , { organization = "CPDT-KA"
      , certifications = [ "Certified Professional Dog Trainer - Knowledge Assessed" ]
      }
    , { organization = "Association Of Professional Dog Trainers"
      , certifications = [ "Professional Member" ]
      }
    , { organization = "C.L.A.S.S."
      , certifications = [ "Canine Life And Social Skills Evaluator" ]
      }
    , { organization = "Karen Pryor Academy"
      , certifications = [ "Puppy Start Right Certified Instructor" ]
      }
    ]


viewCertification x { organization, certifications } =
    column
        [ width fill
        , alignTop
        , spacing (whitespace 1)
        ]
        [ viewOrganization organization
        , column
            [ width fill, spacing 8 ]
            (List.map viewCertifcationHeld certifications)
        ]


viewOrganization organization =
    el
        [ width fill
        , alignTop
        , padding (whitespace 1)
        , Font.bold
        , Background.color palette.base
        , Font.color palette.white
        , Font.center
        ]
        (Text.body organization)


viewCertifcationHeld heldCertification =
    el
        [ paddingLeft
        , Font.center
        , width fill
        ]
        (Text.body ("- " ++ heldCertification))


paddingLeft =
    paddingEach
        { top = 0
        , bottom = 0
        , left = whitespace 1
        , right = 0
        }


viewCertifications x =
    column
        [ width fill
        , height fill
        , alignTop
        , spacing (whitespace 3)
        , padding (whitespace 1)
        , Background.color palette.light
        , Font.color palette.black
        , Views.roundedCorners
        , Views.shadow
        ]
        (List.map (viewCertification x) allCertifications)
