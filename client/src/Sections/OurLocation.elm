module Sections.OurLocation exposing (view)

import Data.ContactInfo as Contact exposing (kk9Contact)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Html
import Html.Attributes as HA
import Responsive
import Section
import Text
import Views exposing (palette, whitespace)


view x =
    el [ width fill, padding (whitespace 2), Background.color palette.light ] <|
        row [ spacing (whitespace 2), centerX ]
            [ viewEvent x
            , viewLocation x
            ]


viewEvent x =
    column
        [ Font.color palette.black
        , Background.color palette.white
        , centerX
        , height fill
        , Views.shadow
        , Border.color palette.base
        , Border.width 8
        , Border.rounded 4
        ]
        [ column
            [ Background.color palette.base
            , padding (whitespace 1)
            , width fill
            , Font.center
            , Font.color palette.white
            ]
            [ Text.h2 "Paint Your Pet" x
            ]
        , image [ width (minimum 350 fill) ] { src = "src/Assets/PaintYourPet.jpg", description = "Paint Your Pet" }
        , column [ padding (whitespace 2), spacing (whitespace 1) ]
            [ Text.h3 "Sunday, January 20 @ 4pm" x
            , Text.body "Paint a portrait of your best friend!"
            , Text.body "No skill required. Artist Stephanie Stark will guide you step by step. You will leave with a beautiful finished portrait of your pet."
            , Text.body "Bring the whole family!"
            , Text.body "All supplies included."
            ]
        ]


viewLocation x =
    column
        [ alignTop
        , Font.center
        , width fill
        , height fill
        , spacing (whitespace 2)
        , Background.color palette.base
        , Font.color palette.white
        , padding (whitespace 2)
        ]
        [ el [ centerX ] <| Text.h2 "Our Facility" x
        , el [ centerX ] (html (viewMap x))
        , el [ centerX ] <|
            Text.bodyBlock
                [ "Conveniently located in historic downtown Loveland, our brand new facility hosts group classes, private lessons and events for dogs and owners across Colorado's Front Range."
                ]
        ]


viewMap x =
    Html.iframe
        [ HA.src "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d97166.21005297953!2d-105.13457332349279!3d40.443307485539435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87695387cd943827%3A0xbffd4bd4cd10f763!2sKK9+Dog+Training+LLC!5e0!3m2!1sen!2sus!4v1541208264891"
        , HA.width (mapWidth x)
        , HA.height (mapHeight x)
        , HA.style "border" "0"
        , HA.style "frameborder" "0"
        , HA.title "Location Map"
        ]
        []


mapWidth =
    Responsive.scale ( 600, 1100 ) ( 288, 600 ) >> round


mapHeight =
    Responsive.scale ( 600, 1100 ) ( 225, 450 ) >> round
