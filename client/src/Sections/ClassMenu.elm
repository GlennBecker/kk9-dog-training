module Sections.ClassMenu exposing (view)

import Data.ClassSchedule as Classes exposing (Class, ClassType, Schedule)
import Element exposing (..)
import Element.Background as Background
import Element.Font as Font
import Text
import Views


view : Schedule -> Int -> Element msg
view schedule x =
    column
        [ Font.color Views.palette.white
        , Font.center
        , spacing 16
        , width fill
        ]
        [ viewClasses schedule x
        ]


viewClasses : Schedule -> Int -> Element msg
viewClasses schedule x =
    let
        displayClasses =
            [ Classes.PuppyPrimer, Classes.MannersMatter1, Classes.ShortAndSweet ]
    in
    schedule
        |> (Classes.toList >> List.reverse)
        |> List.filter (\( c, _ ) -> List.member c displayClasses)
        |> List.map (viewClass x)
        |> wrappedRow [ spacing 16, width fill ]


viewClass : Int -> ( Class, List ClassType ) -> Element msg
viewClass x ( class, _ ) =
    column
        [ padding 16
        , width (fill |> minimum 280)
        , spacing 16
        , Background.color Views.palette.base
        , height fill
        , Views.shadow
        ]
        [ paragraph [ Views.fontShadow ] [ Text.h2 (Classes.className class) x ]
        , column [ spacing 16, centerX ] <| List.map selfSelectionLine (Classes.selfSelection class)
        , link
            [ alignBottom, width fill ]
            { url = "/" ++ Classes.toUrlString class
            , label = Views.linkButton ("Learn About " ++ Classes.className class)
            }
        ]


selfSelectionLine string =
    paragraph [ Views.fontShadow ] [ Text.body string ]



-- HELPERS


showEnrolling : List a -> String
showEnrolling xs =
    case xs of
        [] ->
            "Dates TBA"

        _ ->
            String.fromInt (List.length xs) ++ " enrollment dates"
