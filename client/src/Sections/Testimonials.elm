module Sections.Testimonials exposing (view)

import Element exposing (..)
import Element.Background as Background
import Element.Font as Font
import Responsive
import Section
import Svgs
import Text
import Views exposing (palette, whitespace)


type alias Testimonial =
    { customer : String
    , review : String
    , stars : Int
    }


all : List Testimonial
all =
    [ { customer = "Kara W."
      , review = "We had a wonderful experience with Kay. She worked so well with us, as a family. Our kids were involved through the whole process. She made us feel more comfortable with training and our family couldn’t be happier."
      , stars = 5
      }
    , { customer = "Amber"
      , review = "Kay is so friendly and helpful. She provided much needed assistance for both my dogs and their very different issues. I highly recommend her!"
      , stars = 5
      }
    , { customer = "Glenn B."
      , review = "Kay is amazing. Our Border Collie started learning on day one. She clearly understands how different animals respond to different situations, motivations, etc. We could not be more happy with our experience."
      , stars = 5
      }
    , { customer = "Karin D."
      , review = "Kay is amazing, she really connects with dogs immediately, and has great insight , technique, and knowledge."
      , stars = 5
      }
    , { customer = "Trish M."
      , review = "You could not ask for a more perfect trainer. Amazing."
      , stars = 5
      }
    ]



-- VIEW


view x =
    column
        [ width fill, spacing (whitespace 2), padding (whitespace 4), Background.color palette.brown, Font.color palette.white ]
        [ Text.h1 "Testimonials" x
        , column [ centerX, spacing (whitespace 4) ] (viewTestimonials x all)
        ]


viewTestimonials : Int -> List Testimonial -> List (Element msg)
viewTestimonials =
    let
        sorter =
            Responsive.callOut
                [ Responsive.minWidth 800 (List.map viewTestimonial)
                , Responsive.minWidth 1000
                    (breakInto 2
                        >> List.map (List.map viewTestimonial >> row [ centerX, spacing (whitespace 2) ])
                    )
                , Responsive.minWidth 1200
                    (breakInto 3
                        >> List.map (List.map viewTestimonial >> row [ centerX, spacing (whitespace 2) ])
                    )
                ]
                |> Responsive.defaultTo (List.map viewTestimonial >> row [ spacing (whitespace 2), centerX ] >> List.singleton)
    in
    sorter


breakInto : Int -> List a -> List (List a)
breakInto max xs =
    let
        go x ( current, acc ) =
            if List.length current == max then
                ( [ x ], current :: acc )

            else
                ( x :: current, acc )
    in
    List.foldr go ( [], [] ) xs
        |> (\( x, y ) -> List.reverse (x :: y))


viewTestimonial : Testimonial -> Element msg
viewTestimonial testimonial =
    column
        [ width fill, height fill, spacing (whitespace 1) ]
        [ viewStars testimonial.stars
        , el [ Font.center, centerX ] <| Text.body ("\"" ++ testimonial.review ++ "\"")
        , el [ alignBottom, centerX ] <| Text.body ("- " ++ testimonial.customer)
        ]


viewStars : Int -> Element msg
viewStars count =
    row
        [ spacing 8
        , centerX
        ]
        (List.repeat count viewStar)


viewStar : Element msg
viewStar =
    el [ width (px 25), height (px 25), Font.color palette.accent ] (html Svgs.star)
