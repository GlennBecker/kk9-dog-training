module Sections.FreeLessonBanner exposing (Config, TimeSlot(..), container, errorOrLabel, input, toString, view)

import Data.ContactInfo as ContactInfo
import Data.FormState as FormState exposing (FormState(..))
import Element exposing (..)
import Element.Background as Background
import Element.Font as Font
import Element.Input as Input
import Responsive
import Text
import Views exposing (palette)


type TimeSlot
    = Six
    | SixThirty


toString timeSlot =
    case timeSlot of
        Six ->
            "6:00pm"

        SixThirty ->
            "6:30pm"


type alias RSVP r =
    { r
        | first : String
        , last : String
        , email : String
        , time : TimeSlot
        , subscribe : Bool
    }


type alias Config msg =
    { onFirst : String -> msg
    , onLast : String -> msg
    , onEmail : String -> msg
    , onSubmit : msg
    , onSubscribe : Bool -> msg
    , onTime : TimeSlot -> msg
    }



-- VIEW


view : Config msg -> FormState String -> RSVP r -> Int -> Element msg
view config state rsvp x =
    case state of
        Filling errs ->
            container
                x
                [ el [ Views.fontShadow, centerX ] <| Text.h3 "Free private lesson for you and your dog!" x
                , el [ Views.fontShadow, centerX ] <| Text.h1 "Click & Learn" x
                , el [ Views.fontShadow, centerX ] <|
                    Text.bodyBlock
                        [ "Bring your dog to KK9 to meet your trainer and learn the basics of positive reinforcement training."
                        , "Two free sessions every Monday at 6:00pm & 6:30pm."
                        , "RSVP required."
                        ]
                , row [ spacing 16, centerX, width (maximum 480 fill) ]
                    [ input config.onFirst "First Name" rsvp.first errs
                    , input config.onLast "Last Name" rsvp.last errs
                    ]
                , el [ centerX, width (maximum 480 fill) ] <| input config.onEmail "Email" rsvp.email errs
                , timeSlotPicker config rsvp.time x
                , el [ width (maximum 480 fill), centerX ] <| Views.actionButton config.onSubmit "Request a free lesson!"
                , row [ spacing 8, centerX ]
                    [ Input.checkbox []
                        { onChange = config.onSubscribe
                        , checked = rsvp.subscribe
                        , icon = Input.defaultCheckbox
                        , label = Input.labelHidden "Subscribe to Newsletter"
                        }
                    , text "Receive updates from KK9 Dog Training"
                    ]
                ]

        Sending ->
            container
                x
                [ Text.h1 "Great!" x
                , Text.h3 "Just one moment..." x
                ]

        Sent ->
            container
                x
                [ Text.h1 "Thanks for requesting a lesson!" x
                , Text.h3 "We will be in touch shortly to schedule a date." x
                ]

        SendingError _ ->
            container
                x
                [ Text.h1 "Whoops!" x
                , Text.h3 "There was an error processing your request." x
                , el [ centerX ] <| Text.body <| "Feel free to contact us at " ++ ContactInfo.printPhoneNumber ContactInfo.kk9Contact.phone ++ " or"
                , el [ width (maximum 480 fill), centerX ] <| Views.actionButton config.onSubmit "Retry"
                ]


container x =
    column
        [ spacing 24
        , centerX
        , paddingXY 16 (Basics.round <| Responsive.scale ( 450, 1920 ) ( 16, 104 ) x)
        , width fill
        , Font.center
        , Background.image "/src/Assets/consistentcommunication.jpg"
        , Font.color palette.white
        ]


input toMsg label text errors =
    Input.text [ spacing 16, centerX, Font.color palette.black, spacing 8 ]
        { onChange = toMsg
        , text = text
        , placeholder = Just (Input.placeholder [ Font.alignLeft ] (Element.text label))
        , label = errorOrLabel label errors
        }


errorOrLabel : String -> List ( String, String ) -> Input.Label msg
errorOrLabel field errors =
    let
        error =
            errors
                |> List.filter (\( f, err ) -> field == f)
                |> List.head
    in
    case error of
        Just ( _, errorMessage ) ->
            Input.labelAbove [ Font.color (rgb 1 0 0), Font.alignLeft, width fill ] (Text.bodyUnclamped errorMessage)

        Nothing ->
            Input.labelHidden field


timeSlotPicker config timeSlot x =
    Input.radioRow
        [ spacing 32, centerX, Font.center ]
        { label = Input.labelHidden "Which time would you prefer?"
        , onChange = config.onTime
        , options =
            List.map
                (\time -> Input.option time (Text.h3 (toString time) x))
                [ Six, SixThirty ]
        , selected = Just timeSlot
        }
        |> el [ centerX ]
