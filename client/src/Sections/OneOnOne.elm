module Sections.OneOnOne exposing (Config, view)

import Data.ContactInfo as Contact exposing (kk9Contact)
import Data.FormState as FormState exposing (FormState(..))
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Lazy as Lazy
import Html exposing (Html)
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Responsive
import Section
import Text
import Views exposing (palette, whitespace)


type alias Config msg =
    { onEmail : String -> msg
    , onFirst : String -> msg
    , onLast : String -> msg
    , onSubmit : msg
    }


type alias Form r =
    { r | first : String, last : String, email : String }


view : Config msg -> FormState String -> Form r -> Int -> Element msg
view config state form x =
    Section.view
        palette.base
        palette.white
        [ Text.h1 "Is One-On-One Training Right For You?" x
        , el [ centerX ] <| Text.h2 "How It Works" x
        , Responsive.switch 800
            ( column, row )
            x
            [ spacing (whitespace 4), centerX ]
            (List.map (viewStep x) steps)
        , Responsive.rowToColumn 850
            x
            [ width fill, spacing (whitespace 4) ]
            [ column
                [ alignTop
                , Responsive.switch 850 ( centerX, alignLeft ) x
                , spacing (whitespace 1)
                , width (maximum 650 fill)
                ]
                (whyText x)
            , Lazy.lazy3 (viewRequestForm config) state form x
            ]
        ]
        x


whyText : Int -> List (Element msg)
whyText x =
    [ el [ Responsive.switch 850 ( centerX, alignLeft ) x ] (Text.h3 "Why Private Lessons" x)
    , Text.body "Do you feel anxious about being in a group class situation? Do the days and times for group classes conflict with your schedule? Do you want to work on specific behavior issues? Is your dog reactive? Are you worried that your dog’s reactivity will be a problem in a group class setting?  Private training might be for you!"
    , Text.body "We will make sure your are comfortable in your private sessions. No judgement, no criticism! We are here to help."
    ]


type alias Step =
    { name : String
    , icon : Int
    , description : String
    }


steps =
    [ { name = "Consultation"
      , icon = 1
      , description = "We will meet with you and your dog to discuss concerns / questions and decide the best path forward."
      }
    , { name = "Training"
      , icon = 2
      , description = "Working with you and your dog in a positive learning environment, we will help you develop the necessary skills for success."
      }
    , { name = "Evaluation"
      , icon = 3
      , description =
            "Upon completion, we will help you evaluate your confidence level moving forward."
      }
    ]


viewStep : Int -> Step -> Element msg
viewStep x { name, icon, description } =
    column
        [ alignTop
        , spacing 16
        , width (maximum 450 fill)
        , Responsive.switch 850 ( Font.center, Font.alignLeft ) x
        ]
        [ el [ centerX ] <| Text.h3 name x
        , el [] <| Text.body description
        ]


input toMsg label text errors =
    Input.text [ Font.color palette.black, spacing 8 ]
        { onChange = toMsg
        , text = text
        , placeholder = Just (Input.placeholder [] (Element.text label))
        , label = errorOrLabel label errors
        }


errorOrLabel : String -> List ( String, String ) -> Input.Label msg
errorOrLabel field errors =
    let
        error =
            errors
                |> List.filter (\( f, err ) -> field == f)
                |> List.head
    in
    case error of
        Just ( _, errorMessage ) ->
            Input.labelAbove [ Font.color (rgb 1 0 0), width fill ] (Text.bodyUnclamped errorMessage)

        Nothing ->
            Input.labelHidden field


viewRequestForm config state form x =
    case state of
        Filling errs ->
            column
                [ Responsive.switch 850 ( centerX, alignRight ) x
                , spacing (whitespace 1)
                , padding (whitespace 1)
                , width (maximum 600 fill)
                , height fill
                , Background.color palette.white
                , Views.roundedCorners
                , Views.shadow
                , Font.color palette.black
                ]
                [ row [ spacing 8 ]
                    [ input config.onFirst "First Name" form.first errs
                    , input config.onLast "Last Name" form.last errs
                    ]
                , input config.onEmail "Email" form.email errs
                , el [ width fill, alignBottom ] (Views.actionButton config.onSubmit "Request Information")
                , el
                    [ width fill, Font.center, alignBottom ]
                    (Text.body ("Or Call Us At " ++ Contact.printPhoneNumber kk9Contact.phone))
                ]

        Sending ->
            column
                [ Responsive.switch 850 ( centerX, alignRight ) x
                , spacing (whitespace 1)
                , padding (whitespace 1)
                , width (maximum 600 fill)
                , height fill
                , Background.color palette.white
                , Views.roundedCorners
                , Views.shadow
                , Font.color palette.black
                ]
                [ el [ centerX, centerY, Font.center, width fill ] (Text.h2 "Sending Your Information" x) ]

        Sent ->
            column
                [ Responsive.switch 850 ( centerX, alignRight ) x
                , spacing (whitespace 1)
                , padding (whitespace 1)
                , width (maximum 600 fill)
                , height fill
                , Background.color palette.white
                , Views.roundedCorners
                , Views.shadow
                , Font.color palette.black
                ]
                [ column [ centerX, centerY, spacing 16, width fill, Font.center ]
                    [ Text.h2 "Terrific!" x
                    , Text.h3 "We'll contact your shortly" x
                    ]
                ]

        SendingError _ ->
            column
                [ Responsive.switch 850 ( centerX, alignRight ) x
                , spacing (whitespace 1)
                , padding (whitespace 1)
                , width (maximum 600 fill)
                , height fill
                , Background.color palette.white
                , Views.roundedCorners
                , Views.shadow
                , Font.color palette.black
                ]
                [ column [ centerX, centerY, Font.center, width fill, spacing 16 ]
                    [ Text.h2 "Whoops!" x
                    , Text.h3 "Something went wrong" x
                    , Text.body ("Please contact us at " ++ phoneNumber ++ " or try again.")
                    , Views.actionButton config.onSubmit "Try Again"
                    ]
                ]


phoneNumber =
    Contact.printPhoneNumber kk9Contact.phone
