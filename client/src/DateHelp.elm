module DateHelp exposing (monthAndDay, monthString, printDates, weekDayAbbr, weekDayString, weekdayFromString)

import AssocList as Dict exposing (Dict)
import Date exposing (..)
import Time exposing (Month(..), Weekday(..))


monthString month =
    case month of
        Jan ->
            "Jan"

        Feb ->
            "Feb"

        Mar ->
            "Mar"

        Apr ->
            "Apr"

        May ->
            "May"

        Jun ->
            "June"

        Jul ->
            "July"

        Aug ->
            "Aug"

        Sep ->
            "Sept"

        Oct ->
            "Oct"

        Nov ->
            "Nov"

        Dec ->
            "Dec"


weekDayAbbr =
    weekDayString >> (\x -> String.dropRight (String.length x - 3) x)


weekDayString weekday =
    case weekday of
        Mon ->
            "Monday"

        Tue ->
            "Tuesday"

        Wed ->
            "Wednesday"

        Thu ->
            "Thursday"

        Fri ->
            "Friday"

        Sat ->
            "Saturday"

        Sun ->
            "Sunday"


weekdayFromString string =
    case string of
        "mon" ->
            Just Mon

        "tue" ->
            Just Tue

        "wed" ->
            Just Wed

        "thu" ->
            Just Thu

        "fri" ->
            Just Fri

        "sat" ->
            Just Sat

        "sun" ->
            Just Sun

        _ ->
            Nothing


toMonthDict : List Date -> Dict Month (List Int)
toMonthDict dates =
    let
        folder date dict =
            let
                prevDays =
                    dict
                        |> Dict.get (Date.month date)
                        |> Maybe.withDefault []
            in
            Dict.insert (Date.month date) (Date.day date :: prevDays) dict
    in
    List.foldr folder Dict.empty dates


printDates =
    toMonthDict
        >> Dict.foldr (\m d acc -> printDatesHelp m d :: acc) []
        >> String.join " & "


printDatesHelp month days =
    case days of
        [] ->
            ""

        _ ->
            monthString month ++ " " ++ printDatesWithAnd days


printDatesWithAnd dates =
    case dates of
        [] ->
            ""

        [ x ] ->
            String.fromInt x

        _ ->
            List.map String.fromInt dates
                |> String.join ", "


monthAndDay date =
    monthString (Date.month date) ++ " " ++ String.fromInt (Date.day date)
