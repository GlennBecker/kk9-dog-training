module Urls exposing (enroll)

import Data.ClassSchedule as Class exposing (Class(..), ClassType)
import Date exposing (Date)
import Html
import Url.Builder


enroll : Class -> ClassType -> String
enroll class date =
    Url.Builder.absolute
        [ Class.toUrlString class ]
        [ Url.Builder.string "enroll" (Class.classTypeQuery date) ]


normalize =
    String.toLower >> String.replace " " ""
