module Main exposing (main)

import Browser
import Browser.Dom as Dom
import Browser.Events
import Browser.Navigation as Nav
import Data.ClassSchedule as Classes
import Date exposing (Date)
import Element exposing (Element, column, fill, height, layout, width)
import Element.Font as Font
import Html exposing (Html)
import Pages.CLASS as CLASS
import Pages.ClassInfo as ClassInfo
import Pages.Corona as Corona
import Pages.Covid19 as Covid19
import Pages.Enrollment as Enrollment
import Pages.ExpiredClass as ExpiredClass
import Pages.Home as Home
import Pages.NotFound as NotFound
import Routing exposing (Page(..))
import Sections.CovidBanner as CovidBanner
import Sections.Footer as Footer
import Sections.Landing as Landing
import Task
import Url



-- FLAGS


type alias Flags =
    { width : Int
    , height : Int
    , year : Int
    , month : Int
    , day : Int
    }



-- MODEL


type alias Model =
    { width : Int
    , height : Int
    , key : Nav.Key
    , page : Page
    , classes : Classes.Schedule
    }


todaysDate : Flags -> Date
todaysDate { year, month, day } =
    Date.fromCalendarDate year (Date.numberToMonth (month + 1)) day


init : Flags -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        today =
            todaysDate flags

        schedule =
            Classes.runningClasses today
    in
    ( { width = flags.width
      , height = flags.height
      , key = key
      , page = Routing.parser schedule url
      , classes = schedule
      }
    , Cmd.none
    )



-- MAIN


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }



-- UPDATE


type Msg
    = NoOp
    | Resize Int Int
    | EnrollmentMsg Enrollment.Msg
    | HomeMsg Home.Msg
    | LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( model.page, msg ) of
        ( _, Resize w h ) ->
            ( { model | width = w, height = h }, Cmd.none )

        ( Enrollment enrollmentModel, EnrollmentMsg enrollmentMsg ) ->
            let
                ( newModel, cmd, done ) =
                    Enrollment.update enrollmentMsg enrollmentModel
            in
            if done then
                ( model, Nav.pushUrl model.key "/" )

            else
                ( { model | page = Enrollment newModel }, Cmd.map EnrollmentMsg cmd )

        ( Home home, HomeMsg homeMsg ) ->
            let
                ( newHome, cmd ) =
                    Home.update homeMsg home
            in
            ( { model | page = Home newHome }, Cmd.map HomeMsg cmd )

        ( _, LinkClicked urlRequest ) ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        ( _, UrlChanged url ) ->
            ( { model | page = Routing.parser model.classes url }
            , Task.perform (always NoOp) (Dom.setViewport 0 0)
            )

        _ ->
            ( model, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : a -> Sub Msg
subscriptions _ =
    Browser.Events.onResize Resize



-- VIEW


view : Model -> { title : String, body : List (Html Msg) }
view model =
    let
        { title, page } =
            viewPage model
    in
    { title = title
    , body =
        [ column
            [ width fill
            , height fill
            , Font.family
                [ Font.typeface "Ubuntu"
                , Font.sansSerif
                ]
            ]
            [ CovidBanner.view model.width
            , Landing.view model.width model.height
            , page
            , Footer.view model.width
            ]
            |> layout []
        ]
    }


viewPage : Model -> { page : Element Msg, title : String }
viewPage model =
    case model.page of
        Home home ->
            Home.view model.width model.classes home
                |> mapWith HomeMsg

        Enrollment enrollmentModel ->
            Enrollment.view enrollmentModel model.width
                |> mapWith EnrollmentMsg

        ClassInfo state ->
            ClassInfo.view state model.width
                |> mapWith (\_ -> NoOp)

        CLASS ->
            CLASS.view model.classes model.width

        ClassNotFound class ->
            ExpiredClass.view class model.width

        NotFound ->
            NotFound.view model.width

        Corona ->
            Corona.view model.width

        Covid19 ->
            Covid19.view model.width


mapWith : (msg1 -> msg2) -> { title : String, page : Element msg1 } -> { title : String, page : Element msg2 }
mapWith msg pageInfo =
    { title = pageInfo.title
    , page = Element.map msg pageInfo.page
    }
