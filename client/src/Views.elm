module Views exposing
    ( actionButton
    , fontShadow
    , linkButton
    , palette
    , roundedCorners
    , shadow
    , spacer
    , whitespace
    , withSpacers
    )

import Colors exposing (toColor)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input


type alias Palette =
    { white : Color
    , black : Color
    , accent : Color
    , mid : Color
    , base : Color
    }


palette =
    { white = rgb255 251 251 251
    , black = rgb255 0 0 0
    , accent = toColor Colors.accent
    , base = toColor Colors.base
    , mid = toColor Colors.mid
    , light = toColor Colors.light
    , brown = toColor Colors.brown
    }


whitespace =
    modular 16 1.25 >> round


actionButton : msg -> String -> Element msg
actionButton msg labelText =
    Input.button
        [ padding 15
        , width fill
        , centerX
        , Border.rounded 5
        , Background.color palette.accent
        , Font.color palette.black
        , Border.shadow { offset = ( 0, 4 ), size = 0, blur = 8, color = rgba 0 0 0 0.2 }
        ]
        { onPress = Just msg
        , label = paragraph [ Font.center, centerX, centerY ] [ text labelText ]
        }


linkButton : String -> Element msg
linkButton labelText =
    Input.button
        [ padding 15
        , width fill
        , centerX
        , Border.rounded 4
        , Background.color palette.accent
        , Font.color palette.black
        , Border.shadow { offset = ( 0, 4 ), size = 0, blur = 16, color = rgba 0 0 0 0.2 }
        ]
        { onPress = Nothing
        , label = paragraph [ Font.center, centerX, centerY ] [ text labelText ]
        }


shadow =
    Border.shadow
        { offset = ( 2, 2 )
        , size = 0
        , blur = 2
        , color = rgba 0 0 0 0.35
        }


spacer =
    el [ width fill, height (px 4), Background.color palette.mid ] (text "")


roundedCorners =
    Border.rounded 5


withSpacers x =
    row [ width fill ] [ spacer, x, spacer ]


fontShadow =
    Font.shadow
        { offset = ( 2, 2 )
        , blur = 2
        , color = rgba 0 0 0 0.35
        }
