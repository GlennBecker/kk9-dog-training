module Text exposing (body, bodyBlock, bodyUnclamped, h1, h2, h3, landingHeader, url)

import Element exposing (..)
import Element.Font as Font
import Element.Region as Region
import Responsive
import Views


responsiveText ( small, big ) x =
    round <| Responsive.scale ( 450, 950 ) ( small, big ) x


em x =
    16 * x


landingHeader string x =
    paragraph
        [ Font.family
            [ Font.external
                { url = "https://fonts.googleapis.com/css?family=Zilla+Slab:700"
                , name = "Zilla Slab"
                }
            , Font.serif
            ]
        , Font.size (responsiveText ( em 2, em 5 ) x)
        , width fill
        , spacing 16
        ]
        [ text string
        ]


h1 : String -> Int -> Element msg
h1 string x =
    paragraph
        [ Font.family
            [ Font.typeface "Zilla Slab"
            , Font.serif
            ]
        , Font.size (responsiveText ( em 2, em 3 ) x)
        , Font.center
        , Font.bold
        , Region.heading 1
        ]
        [ text string ]


h2 string x =
    paragraph
        [ Font.family
            [ Font.typeface "Zilla Slab"
            , Font.serif
            ]
        , Font.size (responsiveText ( em 1.625, em 2.25 ) x)
        , Region.heading 2
        ]
        [ text string ]


h3 string x =
    paragraph
        [ Font.size (responsiveText ( em 1.375, em 1.75 ) x), Region.heading 3 ]
        [ text string ]


body x =
    paragraph [ Font.size (em 1), width (maximum 650 fill), spacing 8 ] [ text x ]


bodyBlock =
    List.map body
        >> textColumn [ spacing (Views.whitespace 2), width fill ]


bodyUnclamped x =
    paragraph [ Font.size (em 1), width fill ] [ text x ]


url urlString labelText =
    link [ Font.underline, Font.color Views.palette.accent ] { url = urlString, label = body labelText }
