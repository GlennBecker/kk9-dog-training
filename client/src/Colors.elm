module Colors exposing
    ( Color
    , accent
    , background
    , base
    , black
    , brown
    , font
    , light
    , mid
    , toColor
    , white
    )

import Color
import Color.Generator as Generator
import Element
import Element.Background as Background
import Element.Font as Font


type alias Color =
    Color.Color


base : Color.Color
base =
    Color.fromRGB ( 35, 128, 215 )
        |> Generator.shade 15


accent : Color.Color
accent =
    Generator.complementary base
        |> Generator.tint 20
        |> Generator.tone 85


brown : Color.Color
brown =
    Generator.complementary base
        |> Generator.shade 15
        |> Generator.tone -35


mid : Color.Color
mid =
    base
        |> Generator.tint 25
        |> Generator.tone 10


light : Color.Color
light =
    base
        |> Generator.tint 55


white : Color.Color
white =
    Color.fromHSL ( 0, 0, 100 )


black : Color.Color
black =
    Color.fromHSL ( 0, 0, 0 )


toColor : Color.Color -> Element.Color
toColor color =
    let
        ( r, g, b ) =
            Color.toRGB color
    in
    Element.rgb255 (round r) (round g) (round b)


font =
    toColor >> Font.color


background =
    toColor >> Background.color
