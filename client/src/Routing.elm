module Routing exposing (Page(..), parser)

import Data.ClassSchedule as Schedule
import Date
import DateHelp
import Pages.ClassInfo as ClassInfo
import Pages.Enrollment as Enrollment
import Pages.Home as Home
import Url exposing (Url)
import Url.Parser exposing ((<?>), Parser, map, oneOf, parse, s, top)
import Url.Parser.Query as Query


type Page
    = Home Home.Model
    | ClassInfo ClassInfo.State
    | ClassNotFound Schedule.Class
    | Enrollment Enrollment.Model
    | NotFound
    | CLASS
    | Corona
    | Covid19


parser : Schedule.Schedule -> Url -> Page
parser schedule url =
    parse
        (oneOf
            [ map (enrollmentHelp schedule) (enrollmentParser schedule)
            , map CLASS (s "class")
            , map Corona (s "virtual-appointments")
            , map Covid19 (s "covid-19-update")
            , map (Home Home.init) top
            ]
        )
        url
        |> Maybe.withDefault NotFound


classUrlParser : Schedule.Schedule -> Parser (Schedule.Class -> c) c
classUrlParser schedule =
    schedule
        |> Schedule.classes
        |> List.map (\x -> map x (s (Schedule.toUrlString x)))
        |> oneOf


enrollmentParser : Schedule.Schedule -> Parser (Schedule.Class -> Maybe String -> c) c
enrollmentParser schedule =
    classUrlParser schedule <?> Query.string "enroll"


enrollmentHelp : Schedule.Schedule -> Schedule.Class -> Maybe String -> Page
enrollmentHelp schedule class mString =
    case mString of
        Nothing ->
            if Schedule.isCLASS class then
                NotFound

            else
                ClassInfo.init class schedule
                    |> ClassInfo

        Just string ->
            case Date.fromIsoString string of
                Ok date ->
                    if Schedule.exists class date schedule then
                        Enrollment (Enrollment.init class (DateHelp.monthAndDay date))

                    else
                        ClassNotFound class

                Err _ ->
                    case Schedule.parseTimeUrl string of
                        Ok ( weekday, time ) ->
                            if Schedule.existsOngoing class ( weekday, time ) schedule then
                                (DateHelp.weekDayString weekday ++ " at " ++ Schedule.timeToString time)
                                    |> Enrollment.init class
                                    |> Enrollment

                            else
                                ClassNotFound class

                        _ ->
                            ClassNotFound class
