module Responsive exposing
    ( MediaQuery(..)
    , callOut
    , defaultTo
    , minWidth
    , rowToColumn
    , scale
    , switch
    )

import Element exposing (column, row)


scale : ( Float, Float ) -> ( Float, Float ) -> Int -> Float
scale ( aMin, aMax ) ( bMin, bMax ) a =
    if toFloat a <= aMin then
        bMin

    else if toFloat a >= aMax then
        bMax

    else
        let
            deltaA =
                (toFloat a - aMin) / (aMax - aMin)
        in
        (deltaA * (bMax - bMin)) + bMin


switch : number -> ( a, a ) -> number -> a
switch aMax ( bMin, bMax ) a =
    if a <= aMax then
        bMin

    else
        bMax


rowToColumn breakpoint =
    switch breakpoint ( column, row )


type MediaQuery a
    = MediaQuery (Int -> Maybe a)


query : a -> MediaQuery a
query x =
    MediaQuery (\_ -> Just x)


badQuery =
    MediaQuery (\_ -> Nothing)


defaultTo : a -> MediaQuery a -> (Int -> a)
defaultTo x (MediaQuery f) =
    \width -> Maybe.withDefault x (f width)


orElse : MediaQuery a -> MediaQuery a -> MediaQuery a
orElse (MediaQuery f) (MediaQuery g) =
    MediaQuery <|
        \width ->
            f width
                |> Maybe.map Just
                |> Maybe.withDefault (g width)


minWidth minimum value =
    MediaQuery <|
        \width ->
            if width <= minimum then
                Just value

            else
                Nothing


callOut : List (MediaQuery a) -> MediaQuery a
callOut =
    List.foldr orElse badQuery
