module Pages.Enrollment exposing (Model, Msg, init, update, view)

import Data.ClassSchedule as ClassSchedule
import Data.ContactInfo exposing (kk9Contact)
import Data.EnrollmentForm as EnrollmentForm exposing (Form)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import Element.Lazy as Lazy
import Http
import Responsive
import Section
import Svgs
import Text
import Validate
import Validators as V
import Views exposing (palette, whitespace)


type State
    = FillingOut
    | Sending (Validate.Valid Form)
    | SendingError (Validate.Valid Form) Http.Error
    | Sent


type alias Model =
    { state : State
    , form : Form
    , class : ClassSchedule.Class
    , start : String
    , errors : List ( FieldError, String )
    }


init : ClassSchedule.Class -> String -> Model
init class start =
    { state = FillingOut
    , class = class
    , start = start
    , form = EnrollmentForm.blank
    , errors = []
    }


clearDogInfo : Form -> Form
clearDogInfo form =
    { form
        | dogsName = ""
        , dogsAge = Tuple.mapFirst (\_ -> "") form.dogsAge
        , veterinarian = ""
        , behaviorIssues = ""
        , hasVaccinationPapers = False
    }


type Msg
    = UserTyped TextField String
    | UserChecked CheckBox Bool
    | ServerResponse (Validate.Valid Form) (Result Http.Error ())
    | UserClickedAge EnrollmentForm.AgeUnit
    | Retry (Validate.Valid Form)
    | NewDog
    | Finish
    | Send
    | NoOp


update : Msg -> Model -> ( Model, Cmd Msg, Bool )
update msg model =
    case msg of
        UserTyped field value ->
            ( { model | form = updateText field value model.form }, Cmd.none, False )

        UserChecked field value ->
            ( { model | form = updateCheck field value model.form }, Cmd.none, False )

        UserClickedAge unit ->
            let
                form =
                    model.form

                newForm =
                    { form | dogsAge = Tuple.mapSecond (always unit) form.dogsAge }
            in
            ( { model | form = newForm }, Cmd.none, False )

        Send ->
            case Validate.validate formValidator model.form of
                Ok valid ->
                    ( { model | state = Sending valid, errors = [] }
                    , enroll model.class model.start valid
                    , False
                    )

                Err errors ->
                    ( { model | state = FillingOut, errors = errors }
                    , Cmd.none
                    , False
                    )

        ServerResponse _ (Ok _) ->
            ( { model | state = Sent }, Cmd.none, False )

        ServerResponse _ _ ->
            ( { model | state = Sent }, Cmd.none, False )

        Retry valid ->
            ( { model | state = Sending valid }, enroll model.class model.start valid, False )

        NewDog ->
            ( { model | state = FillingOut, form = clearDogInfo model.form }, Cmd.none, False )

        Finish ->
            ( init model.class model.start, Cmd.none, True )

        NoOp ->
            ( model, Cmd.none, False )


type TextField
    = DogName
    | AgeDOB
    | VetName
    | BehaviorIssues
    | FirstName
    | LastName
    | Address
    | Unit
    | Zipcode
    | Phone
    | Email


type CheckBox
    = HasVaccinations
    | UseDogPhotos
    | UseOwnerPhotos
    | AgreeToTerms
    | Subscribe


updateText : TextField -> String -> Form -> Form
updateText field value form =
    case field of
        DogName ->
            { form | dogsName = value }

        AgeDOB ->
            { form | dogsAge = Tuple.mapFirst (always value) form.dogsAge }

        VetName ->
            { form | veterinarian = value }

        BehaviorIssues ->
            { form | behaviorIssues = value }

        FirstName ->
            { form | first = value }

        LastName ->
            { form | last = value }

        Address ->
            { form | streetAddress = value }

        Unit ->
            { form | unit = value }

        Zipcode ->
            { form | zipcode = value }

        Phone ->
            { form | phone = value }

        Email ->
            { form | email = value }


updateCheck : CheckBox -> Bool -> Form -> Form
updateCheck field value form =
    case field of
        HasVaccinations ->
            { form | hasVaccinationPapers = value }

        UseDogPhotos ->
            { form | useDogsPhotos = value }

        UseOwnerPhotos ->
            { form | useMyPhotos = value }

        AgreeToTerms ->
            { form | agreeToTerms = value }

        Subscribe ->
            { form | subscribeToNewsletter = value }



-- VIEW


view model x =
    { title = ClassSchedule.className model.class ++ " Enrollment"
    , page = viewHelp model x
    }


viewHelp model x =
    case model.state of
        Sent ->
            Section.view
                palette.mid
                palette.white
                [ column
                    [ spacing (whitespace 3)
                    , centerX
                    , centerY
                    , width (maximum 850 fill)
                    , Font.center
                    ]
                    [ Text.h1 "Thanks For Enrolling!" x
                    , Text.h3 "A confirmation has been sent to your e-mail" x
                    , el [ centerX ] <|
                        Text.body <|
                            "If you do not receive a confirmation please contact Kay Williams by phone at "
                                ++ Data.ContactInfo.printPhoneNumber kk9Contact.phone
                                ++ " or email at kay@kk9dogtraining.com"
                    , Responsive.rowToColumn 400
                        x
                        [ width fill, spacing (whitespace 2) ]
                        [ Views.actionButton NewDog "Enroll Another Dog"
                        , Views.actionButton Finish "Go To Home Page"
                        ]
                    ]
                ]
                x

        _ ->
            el
                [ Background.color palette.mid
                , Font.color palette.base
                , padding (whitespace 2)
                , width fill
                ]
            <|
                column
                    [ spacing (whitespace 3)
                    , centerX
                    , width (maximum 850 fill)
                    ]
                    [ Lazy.lazy2 viewForm model x
                    ]


viewForm : Model -> Int -> Element Msg
viewForm ({ class, form } as model) x =
    column
        [ padding (whitespace 1)
        , spacing (whitespace 1)
        , Views.shadow
        , Views.roundedCorners
        , Font.color palette.black
        , Background.color palette.white
        ]
        [ el
            [ width fill, Font.center, Font.color palette.accent ]
            (Text.h2 (ClassSchedule.className class ++ " Enrollment") x)
        , viewDogInformation model x
        , viewOwnerInformation model x
        , viewOptional model x
        , viewTermsOfService model x
        , viewButton model.state
        ]


viewButton : State -> Element Msg
viewButton state =
    case state of
        Sending _ ->
            Views.actionButton NoOp "Enrolling ..."

        SendingError form _ ->
            Views.actionButton (Retry form) "Whoops! Try again?"

        FillingOut ->
            Views.actionButton Send "Enroll me!"

        Sent ->
            Views.actionButton NoOp "You have enrolled!"


viewDogInformation { form, errors } x =
    viewFormSection (viewDogHeader form.dogsName ++ " Information")
        [ Lazy.lazy3 viewTextField DogName form.dogsName errors
        , viewDogsAge form.dogsAge errors x
        , Lazy.lazy3 viewTextField VetName form.veterinarian errors
        , Lazy.lazy3 viewTextField BehaviorIssues form.behaviorIssues errors
        , Text.bodyUnclamped "I will provide proof of vaccination at first training session."
        , Lazy.lazy3 viewCheckBox HasVaccinations form.hasVaccinationPapers errors
        ]
        x


viewDogsAge ( age, unit ) errors x =
    row [ width fill, spacing (whitespace 1) ]
        [ el [ width (px 85) ] (Lazy.lazy3 viewTextField AgeDOB age errors)
        , row [ width fill, alignBottom, height (px 44) ] (List.map (viewAgeUnit x unit) EnrollmentForm.ageUnits)
        ]


viewAgeUnit x selected unit =
    let
        color =
            if selected == unit then
                palette.mid

            else
                palette.light

        printF =
            Responsive.switch 400 ( EnrollmentForm.ageUnitToAbbr, EnrollmentForm.ageUnitToString ) x
    in
    el
        [ Font.color color
        , centerY
        , Events.onClick (UserClickedAge unit)
        , height fill
        , Element.pointer
        , width fill
        , height fill
        , Border.solid
        , Border.width 1
        , Border.color color
        ]
        (el [ centerX, centerY ] (Text.body (printF unit)))


viewDogHeader name =
    if name == "" then
        "Your Dog's"

    else
        name ++ "'s"


viewOwnerInformation { form, errors } x =
    viewFormSection
        "Your Information"
        [ row [ spacing 16, width fill ]
            [ Lazy.lazy3 viewTextField FirstName form.first errors
            , Lazy.lazy3 viewTextField LastName form.last errors
            ]
        , Lazy.lazy3 viewTextField Address form.streetAddress errors
        , Lazy.lazy3 viewTextField Unit form.unit errors
        , Lazy.lazy3 viewTextField Zipcode form.zipcode errors
        , Lazy.lazy3 viewTextField Email form.email errors
        , Lazy.lazy3 viewTextField Phone form.phone errors
        ]
        x


viewTermsOfService { form, errors } x =
    viewFormSection
        "Terms Of Service"
        [ Text.bodyUnclamped trainingRelease
        , Lazy.lazy3 viewCheckBox AgreeToTerms form.agreeToTerms errors
        ]
        x


viewOptional { form, errors } x =
    viewFormSection
        "Optional"
        [ Lazy.lazy3 viewCheckBox UseOwnerPhotos form.useMyPhotos []
        , Lazy.lazy3 viewCheckBox UseDogPhotos form.useDogsPhotos []
        , Lazy.lazy3 viewCheckBox Subscribe form.subscribeToNewsletter []
        ]
        x


sectionHeading headerText x =
    el
        [ centerX
        , Font.center
        , width fill
        , padding (whitespace 1)
        , Background.color palette.base
        , Font.color palette.white
        ]
        (Text.h3 headerText x)


viewChecked isChecked =
    if isChecked then
        html Svgs.checkSquare

    else
        html Svgs.square


imageRelease noun =
    "KK9 may use photos and/or video of " ++ noun ++ " for marketing and informational ads."


trainingRelease =
    "I give my permission for Kay Williams, owner/trainer of KK9 Dog Training LLC to instruct and coach me and my dog(s) using humane and least aversive techniques for dog training. I understand that any use of prong, choke or electronic collars are not allowed at KK9 Dog Training."


viewTextField : TextField -> String -> List ( FieldError, String ) -> Element Msg
viewTextField field value errors =
    Input.text
        [ width fill ]
        { onChange = UserTyped field
        , text = value
        , placeholder = Nothing
        , label = textFieldLabel field (textFieldToLabel field) errors
        }


textFieldToLabel field =
    case field of
        DogName ->
            "Dog's Name"

        FirstName ->
            "First Name"

        LastName ->
            "Last Name"

        Email ->
            "Email"

        Phone ->
            "Phone"

        VetName ->
            "Veterinarian Or Clinic"

        Zipcode ->
            "Zipcode"

        Address ->
            "Street Address"

        Unit ->
            "Apt/Unit"

        AgeDOB ->
            "Age"

        BehaviorIssues ->
            "Behavior Issues"


textFieldLabel =
    errorOrLabel TextError Input.labelAbove


viewCheckBox : CheckBox -> Bool -> List ( FieldError, String ) -> Element Msg
viewCheckBox field value errors =
    Input.checkbox
        [ width fill ]
        { onChange = UserChecked field
        , icon = viewChecked
        , checked = value
        , label = checkBoxLabel field (checkBoxToLabel field) errors
        }


viewFormSection heading formInput x =
    column
        [ width fill
        , Border.rounded 5
        , Border.width 4
        , Border.color palette.base
        ]
        [ sectionHeading heading x
        , column
            [ width fill
            , padding (whitespace 1)
            , spacing (whitespace 3)
            ]
            formInput
        ]


checkBoxLabel =
    errorOrLabel CheckBoxError Input.labelRight


checkBoxToLabel : CheckBox -> String
checkBoxToLabel field =
    case field of
        AgreeToTerms ->
            "I Agree To Terms Of Service"

        UseDogPhotos ->
            imageRelease "my dog(s)"

        UseOwnerPhotos ->
            imageRelease "me"

        Subscribe ->
            "Subscribe To Our Newsletter For Events, Announcements, and Future Class Dates"

        HasVaccinations ->
            "I Will Provide Vaccination Records."


errorOrLabel :
    (field -> FieldError)
    -> (List (Element.Attribute msg) -> Element msg -> Input.Label msg)
    -> field
    -> String
    -> List ( FieldError, String )
    -> Input.Label msg
errorOrLabel errorType labelPosition field label errors =
    let
        error =
            errors
                |> List.filter (\( f, _ ) -> errorType field == f)
                |> List.head
    in
    case error of
        Just ( _, errorMessage ) ->
            labelPosition [ Font.color (rgb 1 0 0), width fill ] (Text.bodyUnclamped errorMessage)

        Nothing ->
            labelPosition [ Font.bold, Font.color palette.base, width fill ] (Text.bodyUnclamped label)



-- VALIDATION


type FieldError
    = TextError TextField
    | CheckBoxError CheckBox


formValidator =
    Validate.all
        [ Validate.ifBlank .first ( TextError FirstName, "Please Enter First Name" )
        , Validate.ifBlank .last ( TextError FirstName, "Please Enter Last Name" )
        , Validate.ifBlank .streetAddress ( TextError Address, "Please Enter Your Address" )
        , Validate.ifBlank .dogsName ( TextError DogName, "Please Enter Dog's Name" )
        , Validate.ifBlank (.dogsAge >> Tuple.first) ( TextError AgeDOB, "Dog's Age" )
        , Validate.ifBlank .veterinarian ( TextError VetName, "Please Enter Veterinarian's Name" )
        , Validate.ifBlank .email ( TextError Email, "Please Enter Your E-mail" )
        , Validate.ifInvalidEmail .email (\email -> ( TextError Email, email ++ " Is Not A Valid E-Mail" ))
        , Validate.ifBlank .phone ( TextError Phone, "Please Enter Your Phone Number" )
        , V.validate V.usPhoneNumber .phone ( TextError Phone, "Phone Number Must Be 10 Digits" )
        , Validate.ifBlank .zipcode ( TextError Zipcode, "Please Enter Your Zip" )
        , V.validate (V.requireLength 5) .zipcode ( TextError Zipcode, "Invalid Zip" )
        , Validate.ifFalse .hasVaccinationPapers ( CheckBoxError HasVaccinations, "Dog Must Be Vaccinated To Participate" )
        , Validate.ifFalse .agreeToTerms ( CheckBoxError AgreeToTerms, "Please Agree To Terms" )
        ]



-- SEND


enroll : ClassSchedule.Class -> String -> Validate.Valid Form -> Cmd Msg
enroll class start form =
    Http.post
        { url = "/api/submit-enrollment"
        , body = Http.jsonBody (EnrollmentForm.encode class start (Validate.fromValid form))
        , expect = Http.expectWhatever (ServerResponse form)
        }
