module Pages.Corona exposing (view)

import Colors
import Element exposing (Element, fill, maximum)
import Element.Font as Font
import Text
import Views


type alias Content =
    { title : String
    , blurb : String
    , description : List String
    }


content : Content
content =
    { title = "Virtual Online Appointments"
    , blurb = "We offer all our standard appointments virtually!"
    , description =
        [ "During these uncertain times, it's important to follow strict social isolation."
        , "But that doesn't mean your canine companion's development needs to stop."
        , "Now is a perfect time to focus on learning new skills, enrichnment and activities to stave off boredom!"
        , "Follow the link below to learn more about our virtual appointments."
        , "Let's bring your companion to the next level!"
        ]
    }


view : Int -> { title : String, page : Element msg }
view width =
    { title = content.title
    , page = viewHelp content width
    }


viewHelp : Content -> Int -> Element msg
viewHelp { title, blurb, description } width =
    Element.el [ Element.width fill, Element.height fill, Colors.background Colors.mid ] <|
        Element.column
            [ Element.spacing 16
            , Element.height fill
            , Element.padding 16
            , Element.centerX
            , Font.center
            , Element.width (maximum 1280 fill)
            ]
            [ Element.column
                [ Element.width fill
                , Element.padding 32
                , Element.spacing 32
                , Colors.font Colors.white
                , Views.fontShadow
                , Colors.background Colors.base
                ]
                [ Text.h2 title width
                , Text.h3 blurb width
                , Element.el [ Element.centerX ] <| Text.bodyBlock description
                , Element.link [ Element.alignBottom, Element.centerX ]
                    { url = "https://square.site/book/8WAAPT7YWF5PK/kk9-dog-training-loveland-co"
                    , label = Views.linkButton "Book An Appointment"
                    }
                , Element.el [ Element.centerX ] <| Text.body "Or Call Kay to Schedule: (970) 412-7898"
                ]
            ]
