module Pages.Covid19 exposing (view)

import Colors as Color exposing (light, mid, white)
import Element exposing (Element, fill, px)
import Element.Border as Border
import Element.Font as Font
import Views


message : List String
message =
    [ "As you know, KK9 has always had small class sizes. This is a real plus as we reopen and move forward."
    , "With just a few new guidelines and protocols in place we will be well with, and even beyond, compliance with state guidelines."
    , "Please check our website for current class schedules and registartions."
    , "Any upcoming dates that are showing up on the site are accurate and registration is open. Puppy Primer will resume Monday, Apr 27th."
    , "You may email or call me directly for more information"
    , "If you were enrolled in a class that did not complete, you may come to your regularly scheduled class next week."
    , "If you are unsure or would like to wait a bit longer, please let me know."
    , "Don't feel pressured to come back until you are ready."
    , "Should you wish to work one-on-one you may self-schedule here:"
    ]


practices : List String
practices =
    [ "One handler per dog in group classes of 3 or 4 dogs. Two handlers may come with one dog for private sessions or in classes of 2 dogs. Check with me before you come if you are unsure."
    , "As always, I will open the door for you and your dog to enter the facility."
    , "Treats will be portioned into plastic baggies. Take the entire baggie if you need treats."
    , "Surfaces and door handles will be cleaned with either an alcohol or bleach solution meeting guidelines for disinfecting."
    , "Distancing of at least 6 feet will be maintained."
    , "Masks will be worn by all '2-leggeds' while in the the facility."
    , "Anyone with symptoms of respiratory illness may not come to class. If you are feeling unwell AT ALL please do not come to class. Make-up opportunities will be provided."
    , "KK9 Dog Training reserves the right to deny admittance to anyone who appears ill or is unwilling to follow state and local directives for health and safety."
    ]


view : Int -> { title : String, page : Element msg }
view width =
    { title = "A Message Regarding Covid-19"
    , page =
        Element.el [ Element.width fill, Element.height fill, Color.background mid ] <|
            Element.column
                [ Element.paddingXY 16 32
                , Element.width (Element.maximum 1024 Element.fill)
                , Element.centerX
                , Element.spacing 24
                ]
                [ Element.textColumn
                    [ Element.paddingXY 24 48
                    , Color.background white
                    , Element.spacing 24
                    , Element.width fill
                    , Views.shadow
                    , Border.rounded 8
                    ]
                  <|
                    header 32 "Update"
                        :: (List.map (\x -> Element.paragraph [] [ Element.text x ]) message ++ [ selfBookButton ])
                , practicesSectionHeader
                , Element.paragraph [ Font.center, Element.centerX, Element.width (Element.maximum 480 fill) ]
                    [ Element.text "" ]
                , Element.column
                    [ Element.width Element.fill
                    , Element.spacing 24
                    ]
                    (List.indexedMap viewPractice practices)
                ]
    }


practicesSectionHeader : Element msg
practicesSectionHeader =
    Element.column [ Element.clip, Views.shadow, Element.width fill, Border.roundEach { topLeft = 0, topRight = 0, bottomLeft = 8, bottomRight = 8 } ]
        [ header 24 "Our Best Practices"
        , Element.column
            [ Color.background white, Font.center, Element.spacing 24, Element.width fill, Element.paddingXY 24 32 ]
            [ Element.paragraph [ Font.size 24 ] [ Element.text "Your health and safety are the top priority." ]
            , Element.paragraph [] [ Element.text "The following practices will help us maintain the safest possible environment" ]
            ]
        ]


viewPractice : Int -> String -> Element msg
viewPractice int text =
    Element.row
        [ Element.paddingXY 24 32
        , Color.background white
        , Border.rounded 8
        , Element.width Element.fill
        , Element.spacing 16
        , Views.shadow
        ]
        [ viewNumber (int + 1)
        , Element.el [ Element.width fill ] <|
            Element.paragraph
                [ Element.width (Element.maximum 560 fill)
                ]
                [ Element.text text ]
        ]


viewNumber : Int -> Element msg
viewNumber int =
    Element.el
        [ Color.background Color.base
        , Color.font light
        , Element.width (px 48)
        , Element.height (px 48)
        , Element.alignTop
        , Font.heavy
        , Border.rounded 48
        ]
        (Element.el [ Element.centerX, Element.centerY ] <| Element.text (String.fromInt int))


header size text =
    Element.el
        [ Element.paddingXY 16 32
        , Element.width fill
        , Font.heavy
        , Font.size size
        , Font.center
        , Color.background Color.base
        , Color.font white
        ]
    <|
        Element.paragraph [] [ Element.text text ]


selfBookButton =
    Element.link [ Element.alignBottom, Element.centerX ]
        { url = "https://square.site/book/8WAAPT7YWF5PK/kk9-dog-training-loveland-co"
        , label = Views.linkButton "Book An Appointment"
        }
