module Pages.CLASS exposing (view)

import Colors
import Data.ClassSchedule as Class exposing (Class(..), ClassType(..))
import Date
import DateHelp exposing (monthString, printDates, weekDayString)
import Element exposing (Element, fill)
import Element.Font as Font
import Text
import Urls exposing (enroll)
import Views


view : Class.Schedule -> Int -> { title : String, page : Element msg }
view schedule x =
    { title = "C.L.A.S.S."
    , page = viewHelp schedule x
    }


viewHelp : Class.Schedule -> Int -> Element msg
viewHelp schedule x =
    Element.column
        [ Font.center
        , Element.width fill
        , Element.spacing 16
        , Element.padding 16
        , Element.height fill
        , Colors.background Colors.mid
        ]
        [ description x
        , upcomingDates schedule x
        ]


description x =
    Element.column
        [ Colors.background Colors.base
        , Element.width fill
        , Element.paddingXY 16 32
        , Element.spacing 32
        , Colors.font Colors.white
        ]
        [ Element.column [ Element.spacing 16, Element.width fill, Views.fontShadow, Element.centerX ]
            [ Text.h1 "C.L.A.S.S." x
            , Text.h3 "Canine Life And Social Skills" x
            , Element.el [ Element.centerX ] <|
                Text.bodyBlock
                    [ "Curriculum developed by Association of Professional Dog Trainers aligns with AKC S.T.A.R. Puppy, Canine Good Citizen and Therapy Dog Prep."
                    , "There are 4 levels of C.L.A.S.S. your dog can earn."
                    , "Each level has specific benchmarks, homework and assessments."
                    ]
            , Element.el [ Element.centerX, Font.italic ] (Text.body "Dogs and handlers will prepare for the level that is most appropriate and will receive\nindividual coaching.")
            ]
        , classLevels x
        ]


classLevels x =
    if x >= 1900 then
        Element.row [ Element.width fill, Element.spacing 16 ]
            [ classInfo highschool x
            , classInfo bachelors x
            , classInfo masters x
            , classInfo phd x
            ]

    else if x < 980 then
        Element.column [ Element.centerX, Element.width fill, Element.spacing 16 ]
            [ classInfo highschool x
            , classInfo bachelors x
            , classInfo masters x
            , classInfo phd x
            ]

    else
        Element.column [ Element.width fill, Element.spacing 16 ]
            [ Element.row [ Element.width fill, Element.spacing 16 ]
                [ classInfo highschool x
                , classInfo bachelors x
                ]
            , Element.row
                [ Element.width fill, Element.spacing 16 ]
                [ classInfo masters x
                , classInfo phd x
                ]
            ]


classInfo : { title : String, covers : List String } -> Int -> Element msg
classInfo { title, covers } x =
    Element.column
        [ Element.spacing 16
        , Element.width
            (if x < 900 then
                fill

             else
                Element.minimum 450 fill
            )
        , Element.alignTop
        , Element.padding 16
        , Element.height fill
        , Colors.background Colors.white
        , Views.shadow
        , Colors.font Colors.black
        ]
        [ header { color = Colors.base, content = Text.h3 title x }
        , skillList covers
        ]


header { color, content } =
    Element.el
        [ Element.padding 16
        , Element.width fill
        , Colors.background color
        , Colors.font Colors.white
        , Views.fontShadow
        , Views.shadow
        ]
        content


skillList xs =
    Element.column
        [ Font.alignLeft
        , Element.spacing 8
        , Element.centerX
        , Element.width fill
        ]
        (List.map ((++) "• " >> Text.body) xs)


upcomingDates schedule x =
    Element.column
        [ Element.width fill
        , Element.height fill
        , Element.spacing 16
        ]
        [ datesFor CLASS1 schedule x
        , datesFor CLASS2 schedule x
        , examPrep x
        ]


datesFor class schedule x =
    Element.column [ Element.spacing 16, Element.width fill ]
        [ header { color = Colors.brown, content = Text.h2 (Class.className class) x }
        , schedule
            |> Class.getDatesFor class
            |> List.map (viewDate class x)
            |> Element.wrappedRow [ Element.width fill, Element.spacing 16 ]
        ]


viewDate class x date =
    case date of
        Full time start dates makeUp ->
            dateCard
                [ header
                    { color = Colors.base
                    , content = Text.h3 (startingDate start) x
                    }
                , Element.row
                    [ Element.width fill, Element.spacing 16 ]
                    [ Text.h3 (String.fromInt (List.length (start :: dates)) ++ " session class - $150 per dog") x
                    ]
                , Element.el [ Element.centerX ] <| Text.body ("Meets " ++ weekDayString (Date.weekday start) ++ "s at " ++ Class.timeToString time)
                , Element.el [ Element.centerX ] <| Text.body ("Dates: " ++ printDates (start :: dates))
                , Element.el [ Element.centerX ] <| whenJust makeUp viewMakeUp
                , enrollButton class date
                ]

        _ ->
            Element.none


enrollButton class date =
    Element.link
        [ Element.width (Element.maximum 300 fill), Element.centerX, Element.alignBottom ]
        { url = enroll class date, label = Views.linkButton "Enroll" }


startingDate date =
    weekDayString (Date.weekday date) ++ ", " ++ monthAndDay date


monthAndDay date =
    monthString (Date.month date) ++ " " ++ String.fromInt (Date.day date)


viewMakeUp makeUp =
    Text.body ("Make-Up Class: " ++ monthAndDay makeUp)


examPrep x =
    dateCard
        [ header { color = Colors.brown, content = Text.h2 "Exam Prep for C.L.A.S.S. Students" x }
        , header { color = Colors.base, content = Text.h3 "Sundays at 3:00 and 3:30 PM" x }
        , Element.el [ Element.centerX ] <| Text.body "Must be enrolled in CLASS 6-week course."
        , Element.link [ Element.alignBottom, Element.centerX ]
            { url = "https://square.site/book/8WAAPT7YWF5PK/kk9-dog-training-loveland-co"
            , label = Views.linkButton "Register Here"
            }
        ]


dateCard =
    Element.column
        [ Element.spacing 16
        , Element.padding 16
        , Element.width (fill |> Element.minimum 240)
        , Views.shadow
        , Colors.background Colors.white
        , Element.height fill
        ]


whenJust x f =
    Maybe.withDefault Element.none (Maybe.map f x)



-- DATA


highschool =
    { title = "High School / H.S."
    , covers =
        [ "Sit and down"
        , "Come and collar grab"
        , "Off-leash heel"
        , "Basic stay"
        , "Confidence course"
        , "Name game"
        , "Comfort around food bowl"
        , "Meeting strangers"
        , "Body handling"
        ]
    }


bachelors =
    { title = "Basic / B.A."
    , covers =
        [ "Wait at door"
        , "Come and leashing-up manners"
        , "Loose-leash walking and attention"
        , "Meet and greet"
        , "Leave it"
        , "Wait for the food bowl"
        , "Stay"
        , "Settle"
        , "Give and take"
        ]
    }


masters =
    { title = "Intermediate / M.A."
    , covers =
        [ "Wait in the car"
        , "Calmly pass by other dogs"
        , "Wait at the door with distraction"
        , "Come and leashing-up manners with distraction"
        , "Sit, down and stand"
        , "Body handling"
        , "Loose-leash walking and leave-it"
        , "Stay with distraction and distance"
        , "Out of sight, out of mind (with hand targeting)"
        ]
    }


phd =
    { title = "Advanced skills / Ph.D."
    , covers =
        [ "Advanced loose-leash walking"
        , "Back-up"
        , "Advanced stay"
        , "Advanced come and leashing-up manners"
        , "Meet and greet with body handling"
        , "Attention during high distraction"
        , "Table manners"
        , "Do you really know sit?"
        ]
    }
