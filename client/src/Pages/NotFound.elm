module Pages.NotFound exposing (view)

import Element exposing (..)
import Text
import Views exposing (whitespace)


view x =
    { title = "404 - Not Found"
    , page =
        column
            [ width fill, height fill, spacing (whitespace 2) ]
            [ el [ centerX, centerY ] <| Text.h1 "Ru-Roh!" x
            , el [ centerX, centerY ] <| Text.h3 "Something went wrong" x
            , el [ centerX, centerY ] <| Text.h3 "The Page You're Looking For Is Unavailable." x
            ]
    }
