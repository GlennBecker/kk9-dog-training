module Pages.ExpiredClass exposing (view)

import Data.ClassSchedule exposing (Class)
import Element exposing (..)
import Text
import Views


view : Class -> Int -> { title : String, page : Element msg }
view class x =
    { title = "Class Not Found"
    , page =
        el [ width fill, height fill ] <|
            column
                [ centerX, centerY, spacing 16 ]
                [ Text.h1 "Whoops!" x
                , Text.h3 "Looks like that class is expired or unavailable" x
                , column [ spacing 8, centerX, width (maximum 320 fill) ]
                    [ link [ centerX, width fill ]
                        { url = Data.ClassSchedule.toUrlString class
                        , label = Views.linkButton ("Go To " ++ Data.ClassSchedule.className class)
                        }
                    , link [ centerX, width fill ]
                        { url = "/"
                        , label = Views.linkButton "Go Home"
                        }
                    ]
                ]
    }
