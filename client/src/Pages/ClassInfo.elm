module Pages.ClassInfo exposing (State, init, view)

import AssocList as Dict exposing (Dict)
import Colors
import Data.ClassSchedule as Schedule exposing (Class, ClassType(..), Schedule)
import Date
import DateHelp exposing (monthString, printDates, weekDayString)
import Element
    exposing
        ( Element
        , alignBottom
        , centerX
        , column
        , el
        , fill
        , height
        , link
        , maximum
        , minimum
        , none
        , padding
        , row
        , spacing
        , text
        , width
        , wrappedRow
        )
import Element.Font as Font
import Text
import Urls exposing (enroll)
import Views


type alias State =
    { class : Class
    , dates : List ClassType
    , subclasses : Dict Class (List ClassType)
    }


init : Class -> Schedule -> State
init class schedule =
    { class = class
    , dates = Schedule.getDatesFor class schedule
    , subclasses = Schedule.getSubclassesFor class schedule
    }



-- VIEW


view : State -> Int -> { title : String, page : Element msg }
view state x =
    { title = Schedule.className state.class
    , page = viewHelp state x
    }



-- CLASS INFO


viewHelp { class, dates, subclasses } x =
    el [ width fill, Colors.background Colors.mid ] <|
        column
            [ spacing 40
            , height fill
            , padding 16
            , centerX
            , Font.center
            , width (maximum 1280 fill)
            ]
        <|
            case Dict.toList subclasses of
                [] ->
                    [ viewClassAndDates class dates x ]

                xs ->
                    viewClassInfo class x
                        :: List.map
                            (\( c, subdates ) ->
                                viewClassAndDates c subdates x
                            )
                            xs


viewClassAndDates class dates x =
    Element.column [ Element.width fill, Element.spacing 8 ]
        [ viewClassInfo class x
        , viewDates class dates x
        ]


viewClassInfo class x =
    let
        description =
            Schedule.describe class
    in
    column
        [ width fill
        , padding 32
        , spacing 32
        , Colors.font Colors.white
        , Views.fontShadow
        , Colors.background Colors.base
        ]
        [ Text.h2 description.title x
        , case description.blurb of
            "" ->
                Element.none

            anything ->
                Text.h3 anything x
        , case description.description of
            [] ->
                Element.none

            xs ->
                el [ centerX ] <| Text.bodyBlock xs
        , if List.isEmpty description.skills then
            Element.none

          else
            el [ centerX ] <| Text.body ("Skills Covered: " ++ viewList description.skills)
        , if List.isEmpty description.includes then
            Element.none

          else
            el [ centerX ] <| Text.body ("Class Includes: " ++ viewList description.includes)
        ]


viewList xs =
    case List.reverse xs of
        [] ->
            ""

        [ x ] ->
            x

        [ a, b ] ->
            a ++ " & " ++ b

        last :: rest ->
            last
                :: " & "
                :: List.intersperse ", " rest
                |> List.reverse
                |> String.join ""


viewDates class dates x =
    case dates of
        [] ->
            column [ centerX ] [ text "More dates to come" ]

        _ ->
            column
                [ spacing 16
                , centerX
                , width fill
                ]
                [ el
                    [ centerX
                    , Colors.font Colors.white
                    , Colors.background Colors.brown
                    , padding 16
                    , width fill
                    ]
                    (Text.h3 "Upcoming Dates" x)
                , wrappedRow [ spacing 16, centerX, width fill ] (List.map (viewDate class x) dates)
                ]


viewDate class x date =
    case date of
        Full time start dates makeUp ->
            dateCard
                [ el
                    [ width fill
                    , padding 16
                    , Colors.background Colors.base
                    , Colors.font Colors.white
                    , Views.fontShadow
                    ]
                  <|
                    Text.h2 (startingDate start) x
                , row
                    [ width fill, spacing 16 ]
                    [ Text.h3 (String.fromInt (List.length (start :: dates)) ++ " session class - $150 per dog") x
                    ]
                , el [ centerX ] <| Text.body ("Meets " ++ weekDayString (Date.weekday start) ++ "s at " ++ Schedule.timeToString time)
                , el [ centerX ] <| Text.body ("Dates: " ++ printDates (start :: dates))
                , el [ centerX ] <| whenJust makeUp viewMakeUp
                , enrollButton class date
                ]

        Mini time start dates ->
            dateCard
                [ bannerHeader (Text.h2 (startingDate start) x)
                , Text.h3 ("Meets " ++ weekDayString (Date.weekday start) ++ "s at " ++ Schedule.timeToString time) x
                , el [ centerX ] <| Text.body (String.fromInt (List.length (start :: dates)) ++ " session class - $75 per dog")
                , el [ centerX ] <| Text.body ("Dates: " ++ printDates (start :: List.reverse dates))
                , enrollButton class date
                ]

        Ongoing time weekday ->
            dateCard
                [ bannerHeader (Text.h2 (weekDayString weekday ++ "s at " ++ Schedule.timeToString time) x)
                , el [ centerX ] <| Text.body (String.fromInt 5 ++ " session class - $150 per dog")
                , el [ centerX ] <| Text.body "Ongoing. Join any time!"
                , enrollButton class date
                ]


dateCard =
    column
        [ spacing 16
        , padding 16
        , height fill
        , width fill
        , Views.shadow
        , Colors.background Colors.white
        , width (minimum 240 fill)
        ]


bannerHeader =
    el
        [ width fill
        , padding 16
        , Colors.background Colors.base
        , Colors.font Colors.white
        , Views.fontShadow
        , height fill
        ]


viewMakeUp makeUp =
    Text.body ("Make-Up Class: " ++ monthAndDay makeUp)


startingDate date =
    weekDayString (Date.weekday date) ++ ", " ++ monthAndDay date


monthAndDay date =
    monthString (Date.month date) ++ " " ++ String.fromInt (Date.day date)


enrollButton class date =
    link
        [ width fill, alignBottom ]
        { url = enroll class date, label = Views.linkButton "Enroll" }



-- HELP


flip f x y =
    f y x


whenJust x f =
    Maybe.map f x
        |> Maybe.withDefault none
