module Pages.Home exposing (Model, Msg, init, update, view)

import Colors
import Data.ClassSchedule as Classes
import Data.FormState exposing (FormState(..))
import Element exposing (..)
import Element.Background as Background
import Element.Font as Font
import Element.Lazy as Lazy
import Http
import Json.Encode as Encode
import Responsive
import Sections.About as About
import Sections.CLASSInfo as CLASSInfo
import Sections.ClassMenu as ClassMenu
import Sections.ContactForm as Contact
import Sections.FreeLessonBanner as FreeLessonBanner exposing (TimeSlot(..))
import Sections.OneOnOne as OneOnOne
import Sections.OneOnOnePackages as OneOnOnePackages
import Sections.Testimonials as Testimonials
import Text
import Validate
import Views exposing (palette)


type alias Model =
    { first : String
    , last : String
    , email : String
    , questions : String
    , subscribe : Bool
    , time : TimeSlot
    , freeLesson : FormState String
    , request : FormState String
    , contact : FormState String
    }


init : Model
init =
    { first = ""
    , last = ""
    , email = ""
    , questions = ""
    , subscribe = True
    , time = Six
    , freeLesson = Filling []
    , request = Filling []
    , contact = Filling []
    }


view : Int -> Classes.Schedule -> Model -> { title : String, page : Element Msg }
view x schedule model =
    { title = "KK9 Dog Training"
    , page =
        column
            [ width fill, Background.color palette.mid, spacing 32 ]
            [ FreeLessonBanner.view freeLessonConfig model.freeLesson model x

            --, newService x
            , column
                [ spacing 16, paddingXY 16 0, centerX, width fill ]
                [ Element.el
                    [ width fill, padding 16, Font.center, Views.fontShadow, Colors.background Colors.brown, Colors.font Colors.white ]
                    (Text.h2 "Which Class Is The Best Fit For You And Your Dog?" x)
                , CLASSInfo.view x
                , ClassMenu.view schedule x
                , Lazy.lazy OneOnOnePackages.view x
                , el
                    [ width fill
                    , padding 16
                    , Views.fontShadow
                    , Colors.font Colors.white
                    , Background.color Views.palette.brown
                    ]
                    (el [ centerX ] <|
                        paragraph
                            [ Font.center, width fill, centerX ]
                            [ Text.body "All classes taught at our downtown facility. "
                            , Text.url "https://goo.gl/maps/vk64i2uBD8qUQkr26" "Get Directions."
                            ]
                    )
                ]
            , Lazy.lazy3 oneOnOne model.request model x
            , Lazy.lazy About.view x
            , Lazy.lazy Testimonials.view x
            , Lazy.lazy3 contactForm model.contact model x
            ]
    }


newService : Int -> Element msg
newService x =
    column
        [ Font.center
        , Background.image "src/Assets/adoptionoptions.jpg"
        , Font.color (rgb 1 1 1)
        , Views.fontShadow
        , width fill
        , paddingXY 16 (Basics.round <| Responsive.scale ( 450, 1920 ) ( 16, 104 ) x)
        , centerX
        , spacing 32
        ]
        [ Text.h3 "**New service**" x
        , Text.h1 "Training and Enrichment Walks" x
        , Text.h3 "Not just another walk around the block!" x
        , el [ centerX ] <|
            Text.bodyBlock
                [ "Do you feel guilty about your dog spending the day alone while you are working?"
                , "Kay will come to visit and walk with a focus on building loose-leash skills and providing enrichment activities."
                ]
        , Text.h3 "30 minutes: $30.00" x
        , Element.link [ alignBottom, centerX ]
            { url = "https://square.site/book/8WAAPT7YWF5PK/kk9-dog-training-loveland-co"
            , label = Views.linkButton "Book An Appointment"
            }
        ]


oneOnOne =
    OneOnOne.view oneOnOneConfig


contactForm =
    Contact.view contactConfig



-- CONFIGS


freeLessonConfig : FreeLessonBanner.Config Msg
freeLessonConfig =
    { onEmail = TypedEmail
    , onFirst = TypedFirst
    , onLast = TypedLast
    , onSubmit = RequestFreeLesson
    , onSubscribe = Subscribe
    , onTime = PickTime
    }


oneOnOneConfig : OneOnOne.Config Msg
oneOnOneConfig =
    { onEmail = TypedEmail
    , onFirst = TypedFirst
    , onLast = TypedLast
    , onSubmit = Request
    }


contactConfig : Contact.Config Msg
contactConfig =
    { onEmail = TypedEmail
    , onFirst = TypedFirst
    , onLast = TypedLast
    , onQuestion = TypedQuestion
    , onSubmit = Contact
    }



-- UPDATE


type Msg
    = TypedFirst String
    | TypedLast String
    | TypedEmail String
    | TypedQuestion String
    | RSVPed (Result Http.Error ())
    | Contacted (Result Http.Error ())
    | Requested (Result Http.Error ())
    | Contact
    | Request
    | RequestFreeLesson
    | PickTime TimeSlot
    | NoOp
    | Subscribe Bool


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        TypedFirst name ->
            ( { model | first = name }, Cmd.none )

        TypedLast name ->
            ( { model | last = name }, Cmd.none )

        TypedEmail email ->
            ( { model | email = email }, Cmd.none )

        TypedQuestion question ->
            ( { model | questions = question }, Cmd.none )

        Contact ->
            case Validate.validate validateContact model of
                Ok valid ->
                    ( { model | contact = Sending }, contact (Validate.fromValid valid) )

                Err errs ->
                    ( { model | contact = Filling errs }, Cmd.none )

        RequestFreeLesson ->
            case Validate.validate validate model of
                Ok valid ->
                    ( { model | freeLesson = Sending }
                    , Cmd.batch [ freeLesson (Validate.fromValid valid) ]
                    )

                Err errs ->
                    ( { model | freeLesson = Filling errs }, Cmd.none )

        Request ->
            case Validate.validate validate model of
                Ok valid ->
                    ( { model | request = Sending }, request (Validate.fromValid valid) )

                Err errs ->
                    ( { model | request = Filling errs }, Cmd.none )

        Subscribe bool ->
            ( { model | subscribe = bool }, Cmd.none )

        RSVPed result ->
            ( { model | freeLesson = handleResponse result }, Cmd.none )

        Requested result ->
            ( { model | request = handleResponse result }, Cmd.none )

        Contacted result ->
            ( { model | contact = handleResponse result }, Cmd.none )

        PickTime timeslot ->
            ( { model | time = timeslot }, Cmd.none )

        NoOp ->
            ( model, Cmd.none )


handleResponse result =
    case result of
        Ok _ ->
            Sent

        Err err ->
            SendingError err



-- ENCODERS


encodeContact : Model -> Encode.Value
encodeContact { first, last, email, questions } =
    Encode.object
        [ ( "name", Encode.string (first ++ " " ++ last) )
        , ( "email", Encode.string email )
        , ( "message", Encode.string questions )
        ]


encodeFreeLesson : Model -> Encode.Value
encodeFreeLesson { first, last, email, time, subscribe } =
    Encode.object
        [ ( "first", Encode.string first )
        , ( "last", Encode.string last )
        , ( "email", Encode.string email )
        , ( "time", Encode.string (FreeLessonBanner.toString time) )
        , ( "subscribe", Encode.bool subscribe )
        ]


encodeRequest : Model -> Encode.Value
encodeRequest { first, last, email } =
    Encode.object
        [ ( "name", Encode.string (first ++ " " ++ last) )
        , ( "email", Encode.string email )
        ]



-- API


contact : Model -> Cmd Msg
contact form =
    Http.post
        { url = "/api/submit-question"
        , body = Http.jsonBody (encodeContact form)
        , expect = Http.expectWhatever Contacted
        }


request : Model -> Cmd Msg
request form =
    Http.post
        { url = "/api/submit-request"
        , body = Http.jsonBody (encodeRequest form)
        , expect = Http.expectWhatever Requested
        }


freeLesson : Model -> Cmd Msg
freeLesson form =
    Http.post
        { url = "/api/submit-freelesson"
        , body = Http.jsonBody (encodeFreeLesson form)
        , expect = Http.expectWhatever RSVPed
        }



-- VALIDATION


validate =
    Validate.all
        [ Validate.ifBlank .first ( "First Name", "Enter First Name" )
        , Validate.ifBlank .last ( "Last Name", "Enter Last Name" )
        , Validate.ifBlank .email ( "Email", "Please Enter Your Email" )
        , Validate.ifInvalidEmail .email (always ( "Email", "Invalid Email" ))
        ]


validateContact =
    Validate.all
        [ Validate.ifBlank .first ( "First Name", "Enter First Name" )
        , Validate.ifBlank .last ( "Last Name", "Enter Last Name" )
        , Validate.ifBlank .email ( "Email", "Please Enter Your Email" )
        , Validate.ifInvalidEmail .email (always ( "Email", "Invalid Email" ))
        , Validate.ifBlank .questions ( "Message", "Please Add A Message" )
        ]
