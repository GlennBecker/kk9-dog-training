module Example exposing (suite)

import Data.ClassSchedule exposing (getClassDates)
import Data.FreeClass as FreeClass
import Date
import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, intRange, list, string)
import Set
import Test exposing (..)


suite : Test
suite =
    [ freeClassProps
    , groupClassProps
    ]
        |> describe "Test Suite"


day =
    List.range 1 31
        |> List.map Fuzz.constant
        |> Fuzz.oneOf


year =
    Fuzz.map ((+) 2019) int


month =
    List.range 1 12
        |> List.map (Date.numberToMonth >> Fuzz.constant)
        |> Fuzz.oneOf


date =
    Fuzz.map3 Date.fromCalendarDate year month day


freeClassProps =
    [ fuzz date "Schedule is never empty" <|
        \d ->
            FreeClass.init d
                |> FreeClass.all
                |> (List.isEmpty >> not)
                |> Expect.true "Empty schedule"
    ]
        |> describe "Free Class Properties"


groupClassProps =
    [ fuzz2 date (list int) "Doesn't include start date" <|
        \start skipped ->
            getClassDates 50 start skipped False
                |> Tuple.first
                |> List.member start
                |> Expect.false "Start date shouldn't be included"
    , fuzz2 date (list int) "All dates unique" <|
        \start skipped ->
            let
                ( dates, _ ) =
                    getClassDates 50 start skipped False

                unique =
                    Set.fromList (List.map Date.toIsoString dates)
            in
            Expect.equal (List.length dates) (Set.size unique)
    , fuzz3 date (intRange 5 50) (list int) "List should be equal to session length - 1" <|
        \start sessions skipped ->
            getClassDates sessions start skipped False
                |> Tuple.first
                |> List.length
                |> Expect.equal (sessions - 1)
    ]
        |> describe "Group Class Properties"
