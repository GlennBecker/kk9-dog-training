FROM microsoft/dotnet:2.1-runtime

RUN useradd -m -s /bin/bash host
USER host
WORKDIR /home/host

COPY --chown=host:host package/ .

CMD dotnet Server.dll