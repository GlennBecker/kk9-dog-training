#!/bin/sh

set -e

apk add curl

REG_NAME=registry.heroku.com
APP_NAME=$1
IMG_NAME=$REG_NAME/$APP_NAME/web

docker login $REG_NAME \
  --username=_ \
  --password=$HEROKU_API_KEY

docker build . \
  --tag $IMG_NAME

docker push $IMG_NAME

IMG_ID=`docker inspect $IMG_NAME --format={{.ID}}`

curl -n -X PATCH https://api.heroku.com/apps/$APP_NAME/formation \
  -d '{"updates":[{"type":"web","docker_image":"'"$IMG_ID"'"}]}' \
  -H "Content-Type: application/json" \
  -H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
  -H "Authorization: Bearer $HEROKU_API_KEY"
