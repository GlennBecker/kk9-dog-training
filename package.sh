#!/bin/sh

# make directories explicitly

mkdir -p package
mkdir -p package/static
mkdir -p package/static/Assets

# copy client files to package directory

cp client/index.html package/static/
cp client/elm.js package/static/
cp client/src/Assets/* package/static/Assets/ -r
cp client/robots.txt package/static/
cp client/2c17de3b1c7e166ca7ac4ccaae4561d3.txt package/static/

# copy server files to package directory

cp server/bin/Debug/netcoreapp2.1/publish/* package/ -r
