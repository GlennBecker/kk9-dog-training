module Webhooks

open FSharp.Data
module E = Encode

let encode first last email time =
  E.object 
    [ ("first", E.string first)
      ("last", E.string last)
      ("email", E.string email)
      ("time", E.string time)
    ]
  |> E.encode

let freeLessonsSheet first last email time =
  let _ =
    Http.Request
      ("https://script.google.com/macros/s/AKfycbxdQ2yUoNlq9qLTBzHvDx_H84cqDNmm3NbMHeeiKTV3Imc3fnI/exec"
      , body = TextRequest (encode first last email time) 
      )
  ()
