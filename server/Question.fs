namespace Kk9

open System
open System.Runtime.Serialization

[<DataContract>]
type Question =
  { [<field: DataMember(Name="name")>]
    name : string
    [<field: DataMember(Name="email")>]
    email : string
    [<field: DataMember(Name="message")>]
    message : string
  }

module Question =

  let ownerEmail question =
    { recipient = Owner
      subject = "New Inquiry"
      body =
        [ "Hello Kay,"
          sprintf "%s writes: " question.name
          sprintf "\"%s\"" question.message
          sprintf "You can contact back at %s." question.email
          "Have a great day!" ]
    }
