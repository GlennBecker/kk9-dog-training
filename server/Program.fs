﻿module Kk9.Program

open System
open Kk9.Enrollment
open Kk9.Mailchimp
open Suave
open Suave.Files
open Suave.Filters
open Suave.Operators
open Suave.Writers
open Mail

let createHandler f (request : HttpRequest) =
  try
    f (Json.fromJson request.rawForm)
    Successful.NO_CONTENT
  with exn ->
    printfn "%s\n" exn.Message
    RequestErrors.BAD_REQUEST (sprintf "%s\n" exn.Message)


let handleFreeLesson = createHandler (fun request -> 
  Mail.send 
    [ FreeClassRsvp.ownerEmail request
      FreeClassRsvp.analyticsEmail request
    ]
  Webhooks.freeLessonsSheet request.first request.last request.email request.time
  if request.subscribe then
    Mailchimp.subscribe request.first request.last request.email
  else
    ()
)


let handleEnrollment = createHandler (fun enrollment ->
  Mail.send [ 
    Enrollment.ownerEmail enrollment
    Enrollment.confirmationEmail enrollment
  ]
  if enrollment.subscribeToNewsletter then
    Mailchimp.subscribe enrollment.first enrollment.last enrollment.email
  else
    ()
)


let handleQuestion =
  createHandler (fun q -> Mail.send [ Question.ownerEmail q ])


let handleConsultation =
  createHandler (fun r -> Mail.send [ Consultation.ownerEmail r])


let handleRsvp = createHandler (fun rsvp ->
  Mail.send [ 
    Rsvp.ownerEmail rsvp
    Rsvp.confirmationEmail rsvp
  ]
)


let noCache =
  setHeader "Cache-Control" "no-cache, no-store, must-revalidate"
  >=> setHeader "Pragma" "no-cache"
  >=> setHeader "Expires" "0"


let app =
  choose
    [ GET >=> choose
        [ path "/elm.js" >=> file "static/elm.js"
          pathScan "/src/Assets/%s" (sprintf "static/Assets/%s" >> file)
          path "/robots.txt" >=> file "static/robots.txt"
          path "/sitemap.xml" >=> file "static/sitemap.xml"
          file "static/index.html"
          path "2c17de3b1c7e166ca7ac4ccaae4561d3.txt" >=> file "static/2c17de3b1c7e166ca7ac4ccaae4561d3.txt"
        ]
      POST >=> choose
        [ path "/api/submit-rsvp" >=> request handleRsvp
          path "/api/submit-enrollment" >=> request handleEnrollment
          path "/api/submit-question" >=> request handleQuestion
          path "/api/submit-request" >=> request handleConsultation
          path "/api/submit-freelesson" >=> request handleFreeLesson
        ]
    ]


let mimeTypes = function
  | ".txt" -> createMimeType "text/plain" true
  | ".xml" -> createMimeType "text/xml" true
  | _ ->
    None


[<EntryPoint>]
let main _argv =
  let port =
    Environment.GetEnvironmentVariable "PORT"
      |> Option.ofObj
      |> Option.defaultValue "8080"
      |> int

  let config =
    { defaultConfig with
        bindings =
          [ HttpBinding.createSimple Protocol.HTTP "0.0.0.0" port
          ]
        mimeTypesMap = defaultMimeTypesMap @@ mimeTypes
    }

  Mail.init ()

  startWebServer config app
  0