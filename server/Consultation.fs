namespace Kk9

open System
open System.Runtime.Serialization

[<DataContract>]
type Consultation =
  { [<field: DataMember(Name="name")>]
    name : String
    [<field: DataMember(Name="email")>]
    email : String
  }

module Consultation =

  let ownerEmail consultation =
    { recipient = Owner
      subject = "New Private Lessons Request!"
      body =
        [ "Hello Kay,"
          sprintf "%s is interested in setting up private lessons." consultation.name
          sprintf "You may contact them at %s." consultation.email
          "Have a great day!" ]
    }
