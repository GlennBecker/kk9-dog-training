namespace Kk9

open System
open System.Runtime.Serialization
open Encode
open FSharp.Data
open FSharp.Data.HttpRequestHeaders

[<DataContract>]
type Subscription =
  { [<field: DataMember(Name = "first")>]
    first : String
    [<field: DataMember(Name = "last")>]
    last : String
    [<field: DataMember(Name = "email")>]
    email : String
  }



module Mailchimp =

  // ENVIRONMENT VARS

  let private env name =
    Environment.GetEnvironmentVariable name  

  let private apiKey =
    env "MAILCHIMP_API_KEY"

  let private dc =
    env "MAILCHIMP_DC"

  let private listID =
    env "MAILCHIMP_LISTID"



  // HELPERS

  let subscriber first last email =
    object
      [ ("email_address", string email)
        ("status", string "subscribed")
        ("merge_fields"
        , object 
            [ ("FNAME", string first)
              ("LNAME", string last)
            ]
        )
      ]


// ADD SUBSCRIBER

  let subscribe first last email =
    let response = 
      Http.Request
        ( sprintf "https://%s.api.mailchimp.com/3.0/lists/%s/members" dc listID
        , headers = 
            [ BasicAuth "website" apiKey
              ContentTypeWithEncoding(HttpContentTypes.Json, Text.Encoding.UTF8)
            ]
        , body = 
            HttpRequestBody.TextRequest(Encode.encode (subscriber first last email))
        )
    printfn "%A" response.Body
    ()