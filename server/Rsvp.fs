namespace Kk9

open System
open System.Runtime.Serialization

[<DataContract>]
type RSVPRequest =
  { [<field: DataMember(Name = "name")>]
    name : String
    [<field: DataMember(Name = "email")>]
    email : String
    [<field: DataMember(Name = "className")>]
    className : String
    [<field: DataMember(Name = "date")>]
    date : String
  }

module Rsvp =

  let ownerEmail rsvp =
    { recipient = Owner
      subject = sprintf "RSVP for %s" rsvp.className
      body =
        [ "Hello Kay,"
          sprintf "%s has rsvp'd for %s on %s." rsvp.name rsvp.className rsvp.date
          sprintf "You may contact them at %s." rsvp.email
          "Have a great day!" ]
    }

  let confirmationEmail rsvp : Email =
    { recipient = Customer rsvp.email
      subject = sprintf "RSVP Confirmation For %s" rsvp.className
      body =
        [ sprintf "Hello %s," rsvp.name
          sprintf "Thank you for rsvping for %s on %s." rsvp.className rsvp.date
          "I look forward to meeting you!"
          "Kay Williams"
          "Owner & Trainer"
          "KK9 Dog Training LLC" ]
    }