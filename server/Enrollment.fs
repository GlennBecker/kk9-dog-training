namespace Kk9

open System
open System.Runtime.Serialization

[<DataContract>]
type EnrollmentForm =
  { [<field: DataMember(Name="className")>]
    className : string
    [<field: DataMember(Name="startDate")>]
    startDate : string
    [<field: DataMember(Name="dogsName")>]
    dogsName : string
    [<field: DataMember(Name="dobOrAge")>]
    dobOrAge : string
    [<field: DataMember(Name="veterinarian")>]
    veterinarian : string
    [<field: DataMember(Name="hasVaccinationPapers")>]
    hasVaccinationPapers : bool
    [<field: DataMember(Name="behaviorIssues")>]
    behaviorIssues : string
    [<field: DataMember(Name="first")>]
    first : string
    [<field: DataMember(Name="last")>]
    last : string
    [<field: DataMember(Name="streetAddress")>]
    streetAddress : string
    [<field: DataMember(Name="unit")>]
    unit : string
    [<field: DataMember(Name="city")>]
    city : string
    [<field: DataMember(Name="zipcode")>]
    zipcode : string
    [<field: DataMember(Name="email")>]
    email : string
    [<field: DataMember(Name="phone")>]
    phone : string
    [<field: DataMember(Name="useMyPhotos")>]
    useMyPhotos : bool
    [<field: DataMember(Name="useDogsPhotos")>]
    useDogsPhotos : bool
    [<field: DataMember(Name="agreeToTerms")>]
    agreeToTerms : bool
    [<field: DataMember(Name="subscribeToNewsletter")>]
    subscribeToNewsletter : bool
  }

module Enrollment =

  let private enrolleeInfo (enrollment : EnrollmentForm) =
    [ sprintf "Owner: %s %s" enrollment.first enrollment.last
      sprintf "Address: %s %s" enrollment.streetAddress enrollment.unit
      sprintf "Zipcode: %s" enrollment.zipcode
      sprintf "Email: %s" enrollment.email
      sprintf "Phone: %s" enrollment.phone
    ]
    |> String.concat "\r\n"


  let private dogInfo (enrollment : EnrollmentForm) =
    [ sprintf "Dog's Name: %s" enrollment.dogsName
      sprintf "Approx. Age: %s" enrollment.dobOrAge
      sprintf "Veterinarian: %s" enrollment.veterinarian
      sprintf "Behavior Issues: %s" enrollment.behaviorIssues
    ]
    |> String.concat "\r\n"


  let private otherInfo (enrollment : EnrollmentForm) =
    [ sprintf "Can Use Owner Image: %b" enrollment.useMyPhotos
      sprintf "Can Use Dog's Image: %b" enrollment.useDogsPhotos
      sprintf "Newsletter Subscriber: %b" enrollment.subscribeToNewsletter
      sprintf "Will provide vaccination records: %b" enrollment.hasVaccinationPapers
      sprintf "I, %s %s, agree to terms of service: %b" enrollment.first enrollment.last enrollment.agreeToTerms
    ]
    |> String.concat "\r\n"


  let ownerEmail enrollment =
    { recipient = Owner
      subject = sprintf "New Enrollment for %s on %s!" enrollment.className enrollment.startDate
      body =
        [ sprintf "%s %s & %s have enrolled in a class!" enrollment.first enrollment.last enrollment.dogsName
          "Their information is attached below."
          sprintf "If you'd like to get in touch with %s, contact at %s or %s" enrollment.first enrollment.phone enrollment.email
          enrolleeInfo enrollment
          dogInfo enrollment
          otherInfo enrollment ]
    }


  let confirmationEmail enrollment =
    { recipient = Customer enrollment.email
      subject = "Thank You For Enrolling!"
      body =
        [ "Thanks for enrolling!"
          "Please remember to bring vaccination records to our first class."
          "We will take your payment at the first class. We accept cash, check, Venmo or credit card."
          sprintf "I look forward to working with you and %s. See you %s!" enrollment.dogsName enrollment.startDate
          "Kay Williams\nOwner & Trainer\nKK9 Dog Training LLC" ]
    }
