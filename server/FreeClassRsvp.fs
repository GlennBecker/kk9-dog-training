namespace Kk9

open System
open System.Runtime.Serialization

[<DataContract>]
type FreeLessonRequest =
  { [<field: DataMember(Name = "first")>]
    first : String
    [<field: DataMember(Name = "last")>]
    last : String
    [<field: DataMember(Name = "email")>]
    email : String
    [<field: DataMember(Name = "time")>]
    time : String
    [<field: DataMember(Name = "subscribe")>]
    subscribe : bool
  }

module FreeClassRsvp =

  let ownerEmail request : Email = 
    { recipient = Owner
      subject = "New free lesson request!"
      body =
        [ "Hello Kay,"
          sprintf "%s %s would like to schedule a free lesson for a %s slot." request.first request.last request.time
          sprintf "You may contact them at %s." request.email
          "Have a great day!" 
        ]
    }

  let analyticsEmail request : Email =
    { recipient = Analytics
      subject = "Free lesson conversion"
      body =
        [ sprintf "A new client requested a free lesson and %s future emails"
            (if request.subscribe then
              "opted into"
            else
              "opted out of"
            )
        ]
    }