module Encode


type Value = 
  Value of string

let encode (Value s) = 
  s



// ENCODERS

let private field (name, (Value x)) =
  sprintf "\"%s\":%s" name x

let string s =
  Value (sprintf "\"%s\"" (System.Web.HttpUtility.JavaScriptStringEncode s))

let int i =
  Value (sprintf "%i" i)

let float f =
  Value (sprintf "%f" f)

let bool b =
  Value (sprintf "%b" b)

let nill =
  Value ("null")

let list f xs =
  xs
  |> List.map (f >> fun (Value v) -> v)
  |> String.concat ","
  |> sprintf "[%s]"
  |> Value

let object xs =
  xs
  |> List.map field 
  |> String.concat ","
  |> sprintf "{%s}"
  |> Value
