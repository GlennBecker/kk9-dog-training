namespace Kk9

open System
open FSharp.Data
module E = Encode

type Recipient =
  | Owner
  | Customer of string
  | Analytics

type Email =
  { recipient: Recipient
    subject: string
    body: List<string> }

module Mail =

  open System
  open System.Net
  open System.Net.Mail


  let private env name =
    Environment.GetEnvironmentVariable name


  let private mailJetKey = 
    env "MAIL_JET_KEY"

  
  let private mailJetSecret = 
    env "MAIL_JET_SECRET"
  
  
  let private username = 
    env "EMAIL_USER_NAME"


  let private analytics =
    "EMAIL_ANALYTICS"


  let private recipient email =
    match email.recipient with
    | Owner ->
      username

    | Customer address ->
      address

    | Analytics ->
      analytics


  let private encodeSender =
    E.object 
      [ ("Email", E.string username)
        ("Name", E.string "KK9 Dog Training")
      ]


  let private encodeRecipient (email : Email) =
    E.object
      [ ("Email", E.string (recipient email))
      ]


  let private encodeBody =
    String.concat "\r\n\r\n" >> E.string


  let private encodeMessage (email: Email) =
    E.object 
      [ ("From", encodeSender)
        ("To", E.list encodeRecipient [email])
        ("Subject", E.string email.subject)
        ("TextPart", encodeBody email.body)
      ]


  let encode (emails : Email list) =
    E.object 
      [ ("Messages", E.list encodeMessage emails)
      ]
    |> E.encode 


  let private sendMailJet (email) =
    let json = (encode email) in
    printf "%s" json 
    Http.RequestString
      ("https://api.mailjet.com/v3.1/send"
      , headers = 
          [ HttpRequestHeaders.BasicAuth mailJetKey mailJetSecret 
            HttpRequestHeaders.ContentType "application/json"
          ]
      , body = TextRequest json
      )


  let rec private processorLoop (inbox : MailboxProcessor<_>) = async {
    let! email = inbox.Receive ()

    let response = sendMailJet email
    printfn "%A" response


    return! processorLoop inbox
  }


  let private processor = new MailboxProcessor<_> (processorLoop)


  let init () =
    processor.Start ()


  let send email =
    processor.Post email
